/*
 * Support Generic Environment Media
 *
 * Copyright (C) 2013 Cambridge Silicon Radio Limited
 *
 * Licensed under the GPL-2 or later.
 */
#include <common.h>
#include <environment.h>
#include <env_generic.h>
#include <asm/io.h>
#include <asm/arch/nand.h>

char *env_name_spec = "MMC";
static int gem_index = 1;
static struct generic_env_media gem[] = {
#ifdef CONFIG_ENV_IS_IN_MMC
	{
		.identity = GEM_MMC,
		.env_init = mmc_env_init,
		.saveenv = mmc_saveenv,
		.env_relocate_spec = mmc_env_relocate_spec,
		.name = "MMC",
	},
#endif
#ifdef CONFIG_ENV_IS_IN_NANDDISK
	{
		.identity = GEM_NANDDISK,
		.env_init = nanddisk_env_init,
		.saveenv = nanddisk_saveenv,
		.env_relocate_spec = nanddisk_env_relocate_spec,
		.name = "NANDDISK",
	},
#endif
#ifdef CONFIG_ENV_IS_IN_SPI_FLASH
	{
		.identity = GEM_SF,
		.env_init = sf_env_init,
		.saveenv = sf_saveenv,
		.env_relocate_spec = sf_env_relocate_spec,
		.name = "SPI Flash",
	},
#endif
};

void set_env_media(void)
{
	unsigned int boot_media;
#ifndef CONFIG_ARCH_ATLAS7
	boot_media = readl(NAND_BASE + NAND_BOOTMODE);
#else
	#define ATLAS7_BOOTMODE_REG 0x1023001c
	boot_media = readl(ATLAS7_BOOTMODE_REG);
#endif
	/*
	 * prima2 and atlas6 have different boot mode value for
	 * NANDDISK boot and MMC boot.
	 */
#if defined(CONFIG_ARCH_ATLASVI) || defined(CONFIG_ARCH_ATLAS7)
	boot_media &= 0x7;
	switch (boot_media) {
	case 0x0:
		gem_index = GEM_NANDDISK;
		break;
	case 0x2:
	case 0x3:
	case 0x7:
		gem_index = GEM_MMC;
		break;
	case 0x5:
		gem_index = GEM_SF;
		break;
	default:
		break;
	}
#elif defined(CONFIG_ARCH_PRIMAII)
	boot_media &= 0xf;
	if (boot_media != 0xd && boot_media != 0xe)
		gem_index = GEM_NANDDISK;
	else
		gem_index = GEM_MMC;
#endif
}
int env_init(void)
{
	int i;
	set_env_media();
	for (i = 0; i < ARRAY_SIZE(gem); i++) {
		if (gem[i].identity == gem_index) {
			strcpy(env_name_spec, gem[i].name);
			return gem[i].env_init();
		}
	}
	return 0;
}

int saveenv(void)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(gem); i++) {
		if (gem[i].identity == gem_index)
			return gem[i].saveenv();
	}
	return 0;
}

void env_relocate_spec(void)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(gem); i++) {
		if (gem[i].identity == gem_index) {
			gem[i].env_relocate_spec();
			break;
		}
	}
}
