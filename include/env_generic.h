/*
 * Support Generic Environment Media
 *
 * Copyright (C) 2013 Cambridge Silicon Radio Limited
 *
 * Licensed under the GPL-2 or later.
 */
#ifndef __ENV_GENERIC_H__
#define __ENV_GENERIC_H__
enum gem_type {
	GEM_MMC,
#ifdef CONFIG_SIRF_NANDDISK
	GEM_NANDDISK,
#endif
	GEM_SF,
};
typedef int (*generic_env_func) (void);
typedef void (*relocate_env_func) (void);
struct generic_env_media {
	int identity;
	generic_env_func saveenv;
	generic_env_func env_init;
	relocate_env_func env_relocate_spec;
	char *name;
};
extern void register_generic_env(int index);
extern void gem_specify_env_name(int index);
extern void mmc_env_relocate_spec(void);
extern int mmc_saveenv(void);
extern int mmc_env_init(void);
extern char *mmc_env_name_spec;

extern void nanddisk_env_relocate_spec(void);
extern int nanddisk_saveenv(void);
extern int nanddisk_env_init(void);
extern char *nanddisk_env_name_spec;

extern void sf_env_relocate_spec(void);
extern int sf_saveenv(void);
extern int sf_env_init(void);
extern char *sf_env_name_spec;
#endif
