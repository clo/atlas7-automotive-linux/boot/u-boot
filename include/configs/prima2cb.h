/*
 * Copyright (C) 2012 Cambridge Silicon Radio Limited
 * Wang Rong<Rong.Wang@csr.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __PRIMA2CB_CONFIG_H
#define __PRIMA2CB_CONFIG_H

/* Copyright Infomation */
#define COPYRIGHT_COMPANY	\
"Copyright (C) 2012 Cambridge Silicon Radio Limited"
#define CONFIG_IDENT_STRING	"\n\n"COPYRIGHT_COMPANY"\n"
#define CONFIG_SYS_PROMPT	"SiRFprima2 # "

#define CONFIG_ARCH_PRIMAII

/*
 * Enable Device Tree Support
 */
#define CONFIG_OF_LIBFDT		1
#define CONFIG_OF_BOARD_SETUP		1
#define CONFIG_SYS_BOOTMAPSZ            (128 << 20)

#ifdef SECURE_BOOT
/* Enable uboot dt support */
#define CONFIG_OF_CONTROL		1
#define CONFIG_OF_SEPARATE		1
#define CONFIG_DEFAULT_DEVICE_TREE	prima2cb

/* Enable fit rsa sign support */
#define CONFIG_RSA			1
#define CONFIG_FIT			1
#define CONFIG_FIT_SIGNATURE		1
#endif

/* kernel image */
#define CONFIG_BOOTM_LINUX			1
#define CONFIG_CMD_BOOTZ			1
#define CONFIG_CMD_SWSUSP			1
#define CONFIG_LZO				1

/* linkage */
#define CONFIG_SKIP_LOWLEVEL_INIT	1
#define CONFIG_SYS_LDSCRIPT		"board/csr/prima2cb/u-boot.lds"

/* hypervisor */
#define CONFIG_SIRF_HYERVISOR

/* Serial */
#define CONFIG_SYS_BAUDRATE_TABLE	{9600, 19200, 38400, 57600, 115200}
#define CONFIG_BAUDRATE			115200
#define CONFIG_SIRF_SERIAL		1

/* Memory */
#define CONFIG_NR_DRAM_BANKS		1
#define CONFIG_SYS_TEXT_BASE		0x00400000
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_TEXT_BASE - 0x5000)

#define CONFIG_SYS_SDRAM_BASE		0x00000000
#define CONFIG_SYS_SDRAM_SIZE		(SDRAM_SIZE * 0x100000)
#define CONFIG_SYS_SDRAM_SIZE_M		SDRAM_SIZE

#undef DO_CS_PATCH
#if (CONFIG_SYS_SDRAM_SIZE_M == 1024)
#define DO_CS_PATCH
#endif

#define CONFIG_SYS_LOAD_ADDR		0x02008000
#define CONFIG_SYS_MEMTEST_START	CONFIG_SYS_SDRAM_BASE
#define CONFIG_SYS_MEMTEST_END		(CONFIG_SYS_SDRAM_BASE + 0x10000000)

#define KERNEL_MEM_BASE			0xC0000000
#define PHYS_DRAM_BASE			0x00000000
#define PHYS_DRAM_SIZE			(SDRAM_SIZE * 0x100000)

/* Mach Type */
#include <asm/mach-types.h>

/* FLASH and environment organization */
#define CONFIG_SYS_NO_FLASH			1

/* Clock */
#define CONFIG_SYS_CLK_FREQ			24000000
#define CONFIG_SYS_HZ				1000

/**
 * #define CONFIG_L2_OFF				1
 * #define CONFIG_SYS_ICACHE_OFF			1
 * #define CONFIG_SYS_DCACHE_OFF			1
 */

#ifndef CONFIG_SYS_L2CACHE_OFF
#define CONFIG_SYS_L2_PL310		1
#define CONFIG_SYS_PL310_BASE		0x80040000
#endif

/* SD/MMC */
#define CONFIG_MMC				1
#define CONFIG_PRIMA2_MMC			1
#define CONFIG_GENERIC_MMC			1
#define CONFIG_SIRF_MMC				1
#define CONFIG_SIRF_MMC0_IDX			0
#define CONFIG_SIRF_MMC1_IDX			1
#define CONFIG_SDHCI				1
#define CONFIG_SIRF_SDHCI			1
#define CONFIG_SIRF_MMC_MAX_CLK			100000000
#define CONFIG_SUPPORT_EMMC_BOOT

/* NAND */
#define CONFIG_SIRF_NANDDISK

#ifdef CONFIG_SIRF_NANDDISK
#define CONFIG_ENV_IS_IN_NANDDISK		1
 /*
  * when use USB MSD to update uboot, we do not know what is the total size
  * of uboot. The update shell will write to this sector to indicate the
  * uboot has been finished to transfer
  */
#define CONFIG_NANDDISK_BOOTZONE_SIZE   (0x200000)
#define CONFIG_NANDDISK_CODE_SIZE       (0x200000)
/*fix at 96M, so that need not rebuild nanddisk.bin for each ram size*/
#define CONFIG_NANDDISK_ENTRY_PADDR	0x06000000
#define CONFIG_NANDDISK_ENTRY_OFFSET	\
	(CONFIG_NANDDISK_ENTRY_PADDR - CONFIG_SYS_SDRAM_BASE)
#define CONFIG_NANDDISK_ENTRY_VADDR	\
	(CONFIG_NANDDISK_ENTRY_OFFSET + KERNEL_MEM_BASE)
#define CONFIG_NANDDISK_DMA_BUF_SIZE	(0x100000)
#define CONFIG_NANDDISK_DMA_BUF_PADDR	\
	(CONFIG_NANDDISK_ENTRY_PADDR + CONFIG_NANDDISK_CODE_SIZE)
#define CONFIG_NANDDISK_DMA_BUF_VADDR	\
	(CONFIG_NANDDISK_ENTRY_VADDR + CONFIG_NANDDISK_CODE_SIZE)
#define CONFIG_CMD_NANDDISK
#define CONFIG_NANDDISK_DEBUG           0
#endif /*CONFIG_SIRF_NANDDISK*/

/* Partition */
#define CONFIG_PARTITIONS			1
#define CONFIG_DOS_PARTITION			1
#define CONFIG_LINUX_ROOT_PARTITION		3

/* USB Host */
#define CONFIG_EHCI_HCD_INIT_AFTER_RESET	1
#define CONFIG_USB_EHCI				1
#define CONFIG_USB_EHCI_SIRF			1
#define CONFIG_USB_STORAGE			1
#define CONFIG_CMD_USB				1

/* USB Device */
#define CONFIG_USB_DEVICE
#define CONFIG_SIRF_UMS
#ifdef CONFIG_SIRF_UMS
/* RAM address definitions, MUST consider the whole memory map */
/* the later 8MB of the 128M SDRAM */
#define USB_RUNTIME_MEM_BASE			(0xc8000000 - 0x00800000)
#define USB_RUNTIME_MEM_SIZE			0x00200000
#endif

/* Size of malloc() pool */
#define CONFIG_ENV_SIZE				(16 << 10)
#define CONFIG_ENV_IS_IN_MMC			1
#define CONFIG_ENV_IS_IN_GENERIC_ENV		1
#define CONFIG_ENV_OFFSET			(0x300000 - 0x10000)
#define CONFIG_SYS_MMC_ENV_DEV			0
#define CONFIG_SYS_MALLOC_LEN			(CONFIG_ENV_SIZE + (2 << 20))

/* scert */
#define CONFIG_SCERT_START			3145728
#define CONFIG_SCERT_SIZE			2097152

#define CONFIG_BOARD_LATE_INIT
/*
 * we always enable swsusp mmc 0:6, if there is a hibernation image,
 * uboot will load it, otherwise, uboot will load cold linux
 */
#define CONFIG_EXTRA_ENV_SETTINGS \
	"bootinf=nanddisk\0" \
	"bootdev=0\0" \
	"bootpart=2\0" \
	"rootpart=5\0" \
	"fdtaddr=0x03608000\0" \
	"imgaddr=0x02008000\0" \
	"bootcfgaddr=0x02007000\0" \
	"meminfo=0\0" \
	"bootcfgfile=/boot.cfg\0"\
	"fdtfile=/dtb\0" \
	"rcvfdtfile=/rcv_dtb\0" \
	"imgfile=/uImage\0" \
	"rcvfile=/recovery.img\0" \
	"kernel_index=0\0" \
	"recovery_index=0\0" \
	"devargs=" \
		"setenv bootargs no_console_suspend retain_initrd " \
		"earlyprintk resumewait " \
		"mem=${meminfo} " \
		"console=ttySiRF1 " \
		"lpj=7995392 " \
		"real_root=/dev/${bootinf}blk${bootdev}p${rootpart} " \
		"root=/dev/${bootinf}blk${bootdev}p${rootpart} " \
		"rootwait " \
		"androidboot.hardware=prima2cb" \
		"block.events_dfl_poll_msecs=2000 " \
		"scert_start=${scert_start} " \
		"scert_size=${scert_size} " \
		"kernel_index=${kernel_index} " \
		"recovery_index=${recovery_index} " \
		"vmalloc=496M " \
		"os_type=${os_type}\0" \
	"devargsse=" \
		"setenv bootargs ${bootargs} module.sig_enforce=1\0" \
	"loadbootcfg=" \
		"load ${bootinf} ${bootdev}:${bootpart} " \
		"${bootcfgaddr} ${bootcfgfile}\0" \
	"loadfdt=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${fdtaddr} ${fdtfile}\0" \
	"loadimage=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${imgaddr} ${imgfile}\0" \
	"loadrcvfdt=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${fdtaddr} ${rcvfdtfile}\0" \
	"loadrcvimage=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${imgaddr} ${rcvfile}\0" \
	"fdtresize=" \
		"fdt addr ${fdtaddr};" \
		"fdt resize\0" \
	"devboot=" \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"run devargs;" \
		"run loadfdt;" \
		"run loadimage;" \
		"run fdtresize;" \
		"bootz ${imgaddr} - ${fdtaddr}\0" \
	"devseboot=" \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"run devargs;" \
		"run devargsse;" \
		"run loadimage;" \
		"bootm ${imgaddr}\0" \
	"devbootrcv=" \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"run devargs;" \
		"run loadrcvfdt;" \
		"run loadrcvimage;" \
		"run fdtresize;" \
		"bootz ${imgaddr} - ${fdtaddr}\0" \
	"devboot_usb=" \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"usb start; " \
		"run devargs;" \
		"run devargsse;" \
		"set bootinf usb; " \
		"set bootpart 1; " \
		"set imgfile /mpImage; " \
		"set fdtfile /dtb; " \
		"run loadfdt;" \
		"run loadimage;" \
		"run fdtresize;" \
		"bootz ${imgaddr} - ${fdtaddr}\0" \
	"devboot_mp=" \
		"set bootpart 1; " \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"run devargs;" \
		"set imgfile /mpImage; " \
		"set fdtfile /dtb; " \
		"run loadfdt;" \
		"run loadimage;" \
		"run fdtresize;" \
		"bootz ${imgaddr} - ${fdtaddr}\0" \
	"devseboot_mp=" \
		"set bootpart 1; " \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"swsusp ${bootinf} ${bootdev}:${resumepart};" \
		"run devargs;" \
		"run devargsse;" \
		"set imgfile /mpImage; " \
		"run loadimage;" \
		"bootm ${imgaddr}\0"

#define CONFIG_KERNEL_INDEX			"kernel_index"
#define CONFIG_RECOVERY_INDEX			"recovery_index"
#define CONFIG_BOOTDELAY			0
#define CONFIG_BOOT_TIMEOUT			30

#define CONFIG_KERNEL_BACKUP			1

#define CONFIG_SECURE_RECOVERY			1

#ifndef SECURE_BOOT
#define CONFIG_BOOTCOMMAND			"run devboot"
#else
#define CONFIG_BOOTCOMMAND			"run devseboot"
#endif


/* kernel image */
#define CONFIG_BOOTM_LINUX			1

/* ATAG */
#define CONFIG_CMDLINE_TAG			1
#define CONFIG_SETUP_MEMORY_TAGS		1
#define CONFIG_INITRD_TAG			1
#define CONFIG_SERIAL_TAG			1

/* Command definition*/
#include <config_cmd_default.h>
#define CONFIG_CMD_MMC				1
#define CONFIG_CMD_STORAGE			1
#define CONFIG_CMD_FS_GENERIC			1
#define CONFIG_CMD_FAT				1
#define CONFIG_CMD_EXT4
#define CONFIG_CMD_EXT4_WRITE
#define CONFIG_CMDLINE_EDITING
#define CONFIG_AUTO_COMPLETE
/* undef to save memory */
#define CONFIG_SYS_LONGHELP

/* Miscellaneous configurable options */
#define CONFIG_SYS_CBSIZE			512
#define CONFIG_SYS_PBSIZE			256
#define CONFIG_SYS_MAXARGS			22

/* Interrupt autoboot even if "bootdelay=0" */
#define CONFIG_ZERO_BOOTDELAY_CHECK

/* Disable network support */
#undef CONFIG_CMD_NET
#undef CONFIG_CMD_NFS

/* Disable console output if "silent" is set */
#define CONFIG_SILENT_CONSOLE
#define CONFIG_SILENT_CONSOLE_UPDATE_ON_RELOC

#endif	/* __PRIMA2CB_CONFIG_H */
