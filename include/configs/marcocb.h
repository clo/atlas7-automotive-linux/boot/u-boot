/*
 * Copyright (C) 2012 Cambridge Silicon Radio Limited
 * Xianglong Du<Xianglong.Du@csr.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __MARCOCB_CONFIG_H
#define __MARCOCB_CONFIG_H

/* Copyright Infomation */
#define COPYRIGHT_COMPANY	\
"Copyright (C) 2012 Cambridge Silicon Radio Limited"
#define CONFIG_IDENT_STRING	"\n\n"COPYRIGHT_COMPANY"\n"
#define CONFIG_SYS_PROMPT	"SiRFmarco # "

/*
 * Enable Device Tree Support
 */
#define CONFIG_OF_LIBFDT		1
#define CONFIG_SYS_BOOTMAPSZ            (128 << 20)

/* linkage */
#define CONFIG_SKIP_LOWLEVEL_INIT	1
#define CONFIG_SYS_LDSCRIPT		"board/csr/common/u-boot.lds"

/* Serial */
#define CONFIG_SYS_BAUDRATE_TABLE	{9600, 19200, 38400, 57600, 115200}
#define CONFIG_BAUDRATE			115200
#define CONFIG_SIRF_SERIAL		1

/* Memory */
#define CONFIG_NR_DRAM_BANKS		1
#define CONFIG_SYS_TEXT_BASE		0x40400000
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_TEXT_BASE - 0x5000)

#define CONFIG_SYS_SDRAM_BASE		0x40000000
#define CONFIG_SYS_SDRAM_SIZE		(SDRAM_SIZE * 0x100000)
#define CONFIG_SYS_SDRAM_SIZE_M		SDRAM_SIZE

#define CONFIG_SYS_BOOT_PARAM_ADDR	0x40000100

#define CONFIG_SYS_LOAD_ADDR		0x42008000
#define CONFIG_SYS_MEMTEST_START	CONFIG_SYS_SDRAM_BASE
#define CONFIG_SYS_MEMTEST_END		(CONFIG_SYS_SDRAM_BASE + 0x10000000)

#define CONFIG_ARCH_MARCO			1
#define CONFIG_SIRFSOC_FPGA			1

/* FLASH and environment organization */
#define CONFIG_SYS_NO_FLASH			1

/* Keep L2 Cache Disabled */
#define CONFIG_L2_OFF				1
#define CONFIG_SYS_ICACHE_OFF			1
#define CONFIG_SYS_DCACHE_OFF			1

/* Clock */
#define CONFIG_SYS_CLK_FREQ			26000000
#define CONFIG_SYS_HZ				1000000
#define CONFIG_IO_CLK_FREQ			26000000
#define CONFIG_CLOCK_TICK_RATE			1000000

/* SD/MMC */
#define CONFIG_MMC				1
//#define CONFIG_MMC_TRACE			1
#define CONFIG_GENERIC_MMC			1
#define CONFIG_SIRF_MMC				1
#define CONFIG_SIRF_MMC_IDX			0
#define CONFIG_SDHCI				1
#define CONFIG_SIRF_SDHCI			1

/* Partition */
#define CONFIG_PARTITIONS			1
#define CONFIG_DOS_PARTITION			1
#define CONFIG_SUPPORT_VFAT			1

/* USB Host */
#define CONFIG_EHCI_HCD_INIT_AFTER_RESET	1
#define CONFIG_USB_EHCI	     			1
#define CONFIG_USB_EHCI_SIRF	        	1
#define CONFIG_USB_STORAGE			1
#define CONFIG_CMD_USB				1

/* Size of malloc() pool */
#define CONFIG_ENV_SIZE				(16 << 10)
#define CONFIG_ENV_IS_NOWHERE			1
#define CONFIG_SYS_MMC_ENV_DEV			0
#define CONFIG_SYS_MALLOC_LEN			(CONFIG_ENV_SIZE + (10 << 20))

#define CONFIG_BOOTARGS_COMMON			"no_console_suspend mem=512M console=ttySiRF1 retain_initrd earlyprintk"
#define CONFIG_BOOTARGS_SD			\
CONFIG_BOOTARGS_COMMON" real_root=/dev/mmcblk0p2 mmc_core.removable=0 resume=/dev/mmcblk0p7"
#define CONFIG_BOOTARGS				CONFIG_BOOTARGS_SD
#define CONFIG_BOOTCOMMAND			"mmc read 0x43608000 0x3780 0x80;mmc read 0x42008000 0x1800 0x1800;fdt addr 0x43608000; fdt resize; bootz 0x42008000 - 0x43608000"
#define CONFIG_BOOTDELAY			3

/* kernel image */
#define CONFIG_BOOTM_LINUX			1
#define CONFIG_CMD_BOOTZ			1

/* ATAG */
#define CONFIG_CMDLINE_TAG			1
#define CONFIG_SETUP_MEMORY_TAGS		1
#define CONFIG_INITRD_TAG			1
#define CONFIG_SERIAL_TAG			1

/* Command definition*/
#include <config_cmd_default.h>
#define CONFIG_CMD_MMC				1
#define CONFIG_CMD_STORAGE			1
#define CONFIG_CMD_FAT				1

/* Miscellaneous configurable options */
#define CONFIG_SYS_CBSIZE			256
#define CONFIG_SYS_PBSIZE			256
#define CONFIG_SYS_MAXARGS			16

#endif	/* __MARCOCB_CONFIG_H */
