/*
 * Copyright (c) 2014-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __ATLAS7CB_CONFIG_H
#define __ATLAS7CB_CONFIG_H

/* Copyright Infomation */
#define COPYRIGHT_COMPANY	\
"Copyright (C) 2012 Cambridge Silicon Radio Limited"
#define CONFIG_IDENT_STRING	"\n"COPYRIGHT_COMPANY"\n"
#define CONFIG_SYS_PROMPT	"SiRF Atlas7DA # "
#define CONFIG_ARCH_ATLAS7
#define CONFIG_HAVE_OWN_RESET

/*
 * use 'silent' to disable console messages
 */
#define CONFIG_SILENT_CONSOLE
#define CONFIG_SILENT_CONSOLE_UPDATE_ON_RELOC

/*
 * Enable Device Tree Support
 */
#define CONFIG_OF_LIBFDT		1
#define CONFIG_OF_BOARD_SETUP		1

/* kernel image */
#define CONFIG_BOOTM_LINUX			1
#define CONFIG_CMD_BOOTZ			1
#define CONFIG_CMD_SWSUSP			1
#define CONFIG_LZO				1

/* linkage */
#define CONFIG_SYS_LDSCRIPT		"board/csr/atlas7cb/u-boot.lds"

/* #define ATLAS7_PXP		1 */
#define CONFIG_KERNEL_BACKUP           1
#define CONFIG_SECURE_RECOVERY         1
#define CONFIG_SYS_L2CACHE_OFF         1
#define CONFIG_LINUX_ROOT_PARTITION    3
/*
 * enable this if we want Linux/Android running in non-security
 * mode with a security backend
 */
#define CONFIG_CSRVISOR_SUPPORT		1

/* Enable Static Voltage Management (SVM) */
#define CONFIG_SVM_SUPPORT		1


#ifdef CONFIG_CSRVISOR_SUPPORT
#define CONFIG_CSRVISOR_PADDR		0x04000000
#define CONFIG_CSRVISOR_VADDR		0x04000000
#define CONFIG_CSRVISOR_MEM_SIZE	(128 * 1024)
#define CONFIG_SECURITY_TLB_ADDR	(CONFIG_CSRVISOR_PADDR + CONFIG_CSRVISOR_MEM_SIZE)
#define CONFIG_ARMV7_NONSEC		1
#define CONFIG_SHA256			1
#endif

/* for EVB, some components have different address with PXP */

/*#define A7DA_M3_SRAM	1*/
#ifdef CONFIG_CSRVISOR_SUPPORT
#define SMP_PEN_ADDR		0x18620468
#define SMP_WAKEMAGIC_ADDR	0x18620464
#define SMP_WAKEMAGIC_VALUE	0x3CAF5D62
#endif

#define CONFIG_ARM_GIC_BASE_ADDRESS 0x10300000

/* Serial */
#define CONFIG_SYS_BAUDRATE_TABLE	{9600, 19200, 38400, 57600, 115200}
#define CONFIG_BAUDRATE			650000
#define CONFIG_SIRF_SERIAL		1

/* io bridge */
#define CONFIG_SIRF_IOBRG            1

/* Memory */
#define CONFIG_NR_DRAM_BANKS		1
/*
 * During sleep/resume process, uboot should not corrupt any saved data which
 * belongs to kernel and drivers, so we have to reserve a dedicated memory
 * range for uboot executing. Now let uboot execute in 2M range(@0x5b800000)
 */
#define UBOOT_MEM_RSV_SIZE			0x200000
#define CONFIG_SYS_TEXT_BASE		0x45800000
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_TEXT_BASE - 0x5000)
#define CONFIG_SYS_SDRAM_BASE		0x40000000
#define CONFIG_SYS_SDRAM_SIZE		(SDRAM_SIZE * 0x100000)
#define CONFIG_SYS_SDRAM_SIZE_M		SDRAM_SIZE
#define CONFIG_SYS_MEM_TOP_HIDE		(CONFIG_SYS_SDRAM_BASE +\
		CONFIG_SYS_SDRAM_SIZE -\
		CONFIG_SYS_TEXT_BASE -\
		UBOOT_MEM_RSV_SIZE)

#define CONFIG_SYS_LOAD_ADDR		0x02008000
#define CONFIG_SYS_MEMTEST_START	CONFIG_SYS_SDRAM_BASE
#define CONFIG_SYS_MEMTEST_END		(CONFIG_SYS_SDRAM_BASE + 0x10000000)

#define KERNEL_MEM_BASE		0xC0000000
#define PHYS_DRAM_BASE		CONFIG_SYS_SDRAM_BASE
#define PHYS_DRAM_SIZE		(SDRAM_SIZE * 0x100000)
#define ATLAS7_DDR_800MHZ 1
#define CONFIG_DO_NOT_RELOCATE	1
/* Mach Type */
#include <asm/mach-types.h>

/* FLASH and environment organization */
#define CONFIG_SYS_NO_FLASH			1

/* Clock */
#define CONFIG_SYS_CLK_FREQ			150000000
#define CONFIG_IO_CLK_FREQ			150000000
#define CONFIG_CLOCK_TICK_RATE			1000000
/* SD/MMC */
#define CONFIG_MMC				1
#define CONFIG_GENERIC_MMC			1
#define CONFIG_SIRF_MMC				1
#define CONFIG_SIRF_MMC0_IDX			0
#define CONFIG_SIRF_MMC1_IDX			1
#define CONFIG_SIRF_MMC2_IDX			2
#define CONFIG_SDHCI				1
#define CONFIG_SIRF_SDHCI			1
#define CONFIG_SIRF_MMC_MAX_CLK			100000000
#define CONFIG_MMC_SDMA				1
#define CONFIG_MMC_ADMA_NA
#define CONFIG_MMC2_ENABLE
#define CONFIG_MMC0_ENABLE

/* NAND */
#define CONFIG_SIRF_NANDDISK
#ifdef CONFIG_SIRF_NANDDISK
 /*
  * when use USB MSD to update uboot, we do not know what is the total size
  * of uboot. The update shell will write to this sector to indicate the
  * uboot has been finished to transfer
  */
#define CONFIG_NANDDISK_BOOTZONE_SIZE	(0x200000)
#define CONFIG_NANDDISK_CODE_SIZE	(0x200000)
/*fix at 96M, so that need not rebuild nanddisk.bin for each ram size*/
#define CONFIG_NANDDISK_ENTRY_PADDR	0x45000000
#define CONFIG_NANDDISK_ENTRY_OFFSET	\
	(CONFIG_NANDDISK_ENTRY_PADDR - CONFIG_SYS_SDRAM_BASE)
#define CONFIG_NANDDISK_ENTRY_VADDR	\
	(CONFIG_NANDDISK_ENTRY_OFFSET + KERNEL_MEM_BASE)
#define CONFIG_NANDDISK_DMA_BUF_SIZE	(0x100000)
#define CONFIG_NANDDISK_DMA_BUF_PADDR	\
	(CONFIG_NANDDISK_ENTRY_PADDR + CONFIG_NANDDISK_CODE_SIZE)
#define CONFIG_NANDDISK_DMA_BUF_VADDR	\
	(CONFIG_NANDDISK_ENTRY_VADDR + CONFIG_NANDDISK_CODE_SIZE)

#define CONFIG_CMD_NANDDISK
#define CONFIG_NANDDISK_DEBUG		0
#endif /*CONFIG_SIRF_NANDDISK*/

/* Partition */
#define CONFIG_PARTITIONS			1
#define CONFIG_DOS_PARTITION			1

/* USB Host */
#define CONFIG_USB_HOST_SUPPORT
#ifdef CONFIG_USB_HOST_SUPPORT
#define CONFIG_USB_MAX_CONTROLLER_COUNT		2
#define CONFIG_EHCI_HCD_INIT_AFTER_RESET	1
#define CONFIG_USB_EHCI				1
#define CONFIG_USB_EHCI_SIRF			1
#define CONFIG_USB_STORAGE			1
#define CONFIG_CMD_USB				1
#endif

/* USB Device */
#define CONFIG_UBOOT_COMMIT_FLAG_ADDR	0x200000
 #define CONFIG_USB_DEVICE
#define CONFIG_SIRF_UMS
#ifdef CONFIG_SIRF_UMS
/* offset 32M within SDRAM */
#define USB_RUNTIME_MEM_BASE			(0x42000000)
#define USB_RUNTIME_MEM_SIZE			0x00200000
#endif


/* #define CONFIG_SIRF_USB_NETWORK */
/* if usb network feature is configured, the UMS will not be available */
#ifdef CONFIG_SIRF_USB_NETWORK
#define CONFIG_CI_UDC
#define CONFIG_USB_GADGET_DUALSPEED
#define CONFIG_USB_ETHER
#define CONFIG_USB_ETH_CDC
#define CONFIG_USB_ETH_RNDIS
#endif

/* ATLAS7 watchdog */
#define CONFIG_HW_WATCHDOG
#define CONFIG_ATLAS7_WATCHDOG      1

/* Size of malloc() pool */
#define CONFIG_ENV_SIZE				(16 << 10)
#define CONFIG_ENV_SECT_SIZE			(64 << 10)
#define CONFIG_ENV_OFFSET			(0x300000 - 0x10000)
#ifdef CONFIG_SIRF_SPI
#define CONFIG_ENV_IS_IN_SPI_FLASH		1
#define CONFIG_ENV_SPI_BUS			1
#define CONFIG_ENV_SPI_CS			0
#define CONFIG_ENV_SPI_MODE			SPI_MODE_0
#endif
#define CONFIG_SYS_MMC_ENV_DEV			0
#define CONFIG_SYS_MALLOC_LEN			(CONFIG_ENV_SIZE + (128 << 10))

/* scert */
#define CONFIG_SCERT_START			3145728
#define CONFIG_SCERT_SIZE			2097152

/* Environment Variable Save*/
#define CONFIG_ENV_IS_IN_MMC			1
#ifdef CONFIG_SIRF_NANDDISK
#define CONFIG_ENV_IS_IN_NANDDISK		1
#endif
#define CONFIG_ENV_IS_IN_GENERIC_ENV		1
#define CONFIG_BOARD_LATE_INIT

/* Secure boot - kernel image signature checking */
/* #define CONFIG_SECURE_BOOT -- default disabled */
#ifdef CONFIG_SECURE_BOOT
#define CONFIG_OF_CONTROL
#define CONFIG_OF_SEPARATE
#define CONFIG_DEFAULT_DEVICE_TREE	atlas7cb

#define CONFIG_RSA
#define CONFIG_DX_SHA1
#define CONFIG_FIT
#define CONFIG_FIT_SIGNATURE
#define CMD_BOOT_IMAGE	"bootm ${imgaddr}\0"
#else
#define CMD_BOOT_IMAGE	"bootz ${imgaddr} - ${fdtaddr}\0"
#endif

/*
 * we always enable swsusp mmc 0:6, if there is a hibernation image,
 * uboot will load it, otherwise, uboot will load cold linux
 */
#define CONFIG_EXTRA_ENV_SETTINGS \
	"bootinf=nand\0" \
	"bootdev=0\0" \
	"rootdev=0\0" \
	"bootpart=2\0" \
	"rootpart=3\0" \
	"csrvisoraddr=0x04000000\0" \
	"fdtaddr=0x40900000\0" \
	"fdt_high=0x45800000\0" \
	"imgaddr=0x40008000\0" \
	"bootcfgaddr=0x40007000\0" \
	"meminfo=0\0" \
	"csrvisorfile=/csrvisor.bin\0" \
	"bootcfgfile=/boot.cfg\0"\
	"fdtfile=/dtb-v1\0" \
	"rcvfdtfile=/rcv_dtb-v1\0" \
	"imgfile=/zImage-v1\0" \
	"rcvfile=/recovery.img-v1\0" \
	"kernel_index=0\0" \
	"recovery_index=0\0" \
	"usbnet_devaddr=00:11:22:33:44:55\0" \
	"usbnet_hostaddr=00:aa:bb:cc:dd:ee\0" \
	"ipaddr=192.168.2.100\0" \
	"serverip=192.168.2.1\0" \
	"netmask=255.255.255.0\0" \
	"tu=tftp 40000000 u-boot.csr; mmc write 40000000 1 400\0" \
	"tk=tftp 40000000 zImage;" \
		"ext4write mmc 0:2 40000000 /zImage-v1 500000\0" \
	"devargs=" \
		"setenv bootargs no_console_suspend retain_initrd " \
		"earlyprintk resumewait " \
		"mem=${meminfo} " \
		"console=ttySiRF1 " \
		"lpj=7995392 " \
		"quiet " \
		"root=/dev/${bootinf}blk${rootdev}p${rootpart} " \
		"rfkill.default_state=0 " \
		"clk_ignore_unused " \
		"usbcore.autosuspend=-1 " \
		"g_ffs.idVendor=0x18d1 g_ffs.idProduct=0x4e26 " \
		"g_ffs.iSerialNumber=0123456789ABCDEF " \
		"rootwait " \
		"kernel_index=${kernel_index} " \
		"recovery_index=${recovery_index} " \
		"androidboot.hardware=sirfsocv7\0" \
	"loadcsrvisor=" \
		"load ${bootinf} ${bootdev}:${bootpart} " \
		"${csrvisoraddr} ${csrvisorfile}\0" \
	"loadcsrvisor_usb=" \
		"usb start; " \
		"set bootinf usb; " \
		"set bootdev 0; " \
		"set bootpart 1; " \
		"load ${bootinf} ${bootdev}:${bootpart} " \
		"0x04000000 ${csrvisorfile}; " \
		"go 0x04000000\0" \
	"loadbootcfg=" \
		"load ${bootinf} ${bootdev}:${bootpart} " \
		"${bootcfgaddr} ${bootcfgfile}\0" \
	"loadfdt=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${fdtaddr} ${fdtfile}\0" \
	"loadimage=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${imgaddr} ${imgfile}\0" \
	"loadsimage=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${imgaddr} ${simgfile}\0" \
	"loadrcvfdt=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${fdtaddr} ${rcvfdtfile}\0" \
	"loadrcvimage=" \
		"load ${bootinf} ${bootdev}:${bootpart} "\
		"${imgaddr} ${rcvfile}\0" \
	"fdtresize=" \
		"fdt addr ${fdtaddr};" \
		"fdt resize\0" \
	"devboot=" \
		"run devargs;" \
		"run loadfdt;" \
		"run loadimage;" \
		CMD_BOOT_IMAGE \
	"devbootrcv=" \
		"run devargs;" \
		"run loadrcvfdt;" \
		"run loadrcvimage;" \
		CMD_BOOT_IMAGE \
	"devboot_usb=" \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"usb start; " \
		"run devargs;" \
		"set bootinf usb; " \
		"set bootpart 1; " \
		"set imgfile /mpImage; " \
		"set fdtfile /dtb; " \
		"run loadfdt;" \
		"run loadimage;" \
		"run fdtresize;" \
		CMD_BOOT_IMAGE \
	"devboot_mp=" \
		"set bootpart 1; " \
		"echo booting from ${bootinf} ${bootdev}:${bootpart}...;" \
		"run devargs;" \
		"set imgfile /mpImage; " \
		"set fdtfile /dtb; " \
		"run loadfdt;" \
		"run loadimage;" \
		"run fdtresize;" \
		CMD_BOOT_IMAGE

#define CONFIG_KERNEL_INDEX			"kernel_index"
#define CONFIG_RECOVERY_INDEX			"recovery_index"
#define CONFIG_BOOTDELAY		        0
#define CONFIG_BOOT_TIMEOUT			20
#define CONFIG_ZERO_BOOTDELAY_CHECK             1
#define CONFIG_NOCFW_ENABLE	1

#ifndef CONFIG_CSRVISOR_SUPPORT
#define CONFIG_BOOTCOMMAND	"run devboot"
#else
#ifdef ATLAS7_PXP
#define CONFIG_BOOTCOMMAND	"set bootinf mmc;set bootpart 1;"\
						"set rootpart 1;run loadcsrvisor;go 0x04000000"
#else
#define CONFIG_BOOTCOMMAND    "run loadcsrvisor;runcv ${filesize}"
#endif
#define CONFIG_BOOTCOMMAND2			"run devboot"
#endif


/* kernel image */
#define CONFIG_BOOTM_LINUX			1

/* ATAG */
#define CONFIG_CMDLINE_TAG			1
#define CONFIG_SETUP_MEMORY_TAGS		1
#define CONFIG_INITRD_TAG			1

/* Command definition*/
#include <config_cmd_default.h>
#undef CONFIG_BOOTM_NETBSD
#undef CONFIG_BOOTM_PLAN9
#undef CONFIG_BOOTM_RTEMS
#undef CONFIG_BOOTM_VXWORKS
#undef CONFIG_GZIP
#undef CONFIG_ZLIB
#undef CONFIG_CMD_FPGA
#undef CONFIG_CMD_IMI
#undef CONFIG_CMD_ITEST
#undef CONFIG_CMD_FLASH
#undef CONFIG_CMD_IMLS
#undef CONFIG_CMD_LOADB
#undef CONFIG_CMD_LOADS
#undef CONFIG_CMD_MISC
#undef CONFIG_CMD_NET
#undef CONFIG_CMD_NFS
#undef CONFIG_CMD_SETGETDCR
#undef CONFIG_CMD_SOURCE
#undef CONFIG_CMD_XIMG

#define CONFIG_CMD_MMC				1
#define CONFIG_CMD_STORAGE			1
#define CONFIG_CMD_FS_GENERIC			1
#define CONFIG_CMD_FAT				1
#define CONFIG_CMD_EXT4
#define CONFIG_CMD_EXT4_WRITE
#define CONFIG_CMDLINE_EDITING
#define CONFIG_AUTO_COMPLETE
#define CONFIG_CMD_IOBG	1
#define CONFIG_CMD_VALIDATION

/* undef to save memory */
#define CONFIG_SYS_LONGHELP

/* Miscellaneous configurable options */
#define CONFIG_SYS_CBSIZE			1024
#define CONFIG_SYS_PBSIZE			1024
#define CONFIG_SYS_MAXARGS			32

/* On-Chip SPI driver support */
/*#define CONFIG_SIRF_SPI*/
#ifdef CONFIG_SIRF_SPI
#define CONFIG_CMD_SF
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_MACRONIX
#define CONFIG_SF_DEFAULT_BUS			1
#define CONFIG_SF_DEFAULT_MODE			SPI_MODE_0
#endif

/* Disable network support */
#ifdef CONFIG_SIRF_USB_NETWORK
#define CONFIG_CMD_NET
#define CONFIG_CMD_PING
#endif

/* OTP menu support */
#define CONFIG_CMD_DXOTP    1

#endif	/* __ATLAS7CB_CONFIG_H */
