/*
 * Copyright (C) 2014  Dialog Semiconductor Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 */

#include "da9061_wdt.h"
#include <watchdog.h>

#define I2C_SUCCESS	(0)
#define PMIC_ERR_I2C	(-1)
#define DA9061_I2C_RETRY_CNT	(3)

/*****************************************************************************/
/* i2c interface wrapping */
static inline int da9061_bsp_i2c_probe(u8 chip)
{
	DLG_DBG("i2c probe - chip[0x%x]\n", chip);
	if (i2c_init(1, 20 * 1000))
		return 0;
	else
		return 1;
/*	return i2c_probe(chip);*/
}

static inline int da9061_bsp_i2c_write(u8 slave_addr,
					int reg, u8 val)
{
	DLG_DBG
	("i2c write slv[0x%x], reg[0x%x], val[0x%x]\n", slave_addr, reg, val);

	if (i2c_write(1, slave_addr, reg, val))
		return 0;
	else
		return 1;
}

static inline int da9061_bsp_i2c_read(u8 slave_addr, int reg, u8 *val)
{
	DLG_DBG
	("i2c read slv[0x%x], reg[0x%x]\n", slave_addr, reg);

	if (i2c_read(1, slave_addr, reg, val))
		return 0;
	else
		return 1;
}
/*****************************************************************************/

static inline int da9061_reg_write(int reg, u8 val)
{
	int ret;
	u8 r_data = val, retry;

	for (retry = 0; retry < DA9061_I2C_RETRY_CNT; retry++) {
		ret = da9061_bsp_i2c_write(DA9061_I2C_SLAVE, reg, val);
		if (ret == I2C_SUCCESS)
			break;
	}
	if (ret != I2C_SUCCESS) {
		printf
		("Failed i2c write - ret=%d data=0x%x\n", ret, r_data);
		return PMIC_ERR_I2C;
	}
	return ret;
}


static inline int da9061_reg_read(int reg, u8 *dest)
{
	int ret;
	u8 r_data, retry;

	for (retry = 0; retry < DA9061_I2C_RETRY_CNT; retry++) {
		ret = da9061_bsp_i2c_read(DA9061_I2C_SLAVE, reg, &r_data);
		if (ret == I2C_SUCCESS)
				break;
	}
	if ((ret != I2C_SUCCESS) || (r_data < 0)) {
		printf("Failed I2C read - ret=%d data=0x%x\n", ret, r_data);
		return PMIC_ERR_I2C;
	}
	*dest = r_data;

	return ret;
}

static inline int da9061_update_bits(int reg, u8 mask, u8 val)
{
	int ret = 1;
	u8 tmp, orig;
	u8 retry;

	for (retry = 0; retry < DA9061_I2C_RETRY_CNT; retry++) {
		ret = da9061_bsp_i2c_read(DA9061_I2C_SLAVE, reg, &orig);
		if (ret == I2C_SUCCESS)
			break;
	}
	if ((ret != I2C_SUCCESS) || (orig < 0)) {
		printf("FAIL I2C read error - ret=%d data=0x%x\n", ret, orig);
		return PMIC_ERR_I2C;
	}

	tmp = orig & ~mask;
	tmp |= val & mask;

	for (retry = 0; retry < DA9061_I2C_RETRY_CNT; retry++) {
		ret = da9061_bsp_i2c_write(DA9061_I2C_SLAVE, reg, tmp);
		if (ret == I2C_SUCCESS)
			break;
	}

	if (ret != I2C_SUCCESS) {
		printf("FAIL I2C write error - ret=%d data=0x%x\n", ret, tmp);
		return PMIC_ERR_I2C;
	}

	return ret;
}

#undef SUPPORT_WATCHDOG_BOOTM
/* this is for only WDT test */

static bool connected = true;

static int da9061_check_fault_log(void)
{
	unsigned char value = 0;

	if (!(da9061_reg_read(DA9061_REG_FAULT_LOG, &value))) {
		DLG_DBG("[%s], fault_log reg value [0x%x]\n", __func__, value);

		/* Report the fault after reboot to the user */
		if (value & 0x80)
			printf("DA9061 Fault Log: WAIT_SHUT\n");
		if (value & 0x40)
			printf("DA9061 Fault Log: nRESETREQ\n");
		if (value & 0x20)
			printf("DA9061 Fault Log: KEY_RESET\n");
		if (value & 0x10)
			printf("DA9061 Fault Log: TEMP_CRIT\n");
		if (value & 0x08)
			printf("DA9061 Fault Log: VDD_START\n");
		if (value & 0x04)
			printf("DA9061 Fault Log: VDD_FAULT\n");
		if (value & 0x02)
			printf("DA9061 Fault Log: POR\n");
		if (value & 0x01)
			printf("DA9061 Fault Log: TWD_ERROR");

		/* Clear the DA9061 FAULT_LOG on start-up */
		value = 0xff;		/* TODO: */
		da9061_reg_write(DA9061_REG_FAULT_LOG, value);
	}

	return 0;
}

static int da9061_check_device_ID(void)
{
	int ret = 0;
	int retry_count = DA9061_I2C_RETRY_CNT;
	int err_count = 0;
	u8 value, slave = DA9061_I2C_SLAVE;

	DLG_DBG("[%s]\n", __func__);

	/* detect the DA9061 */
	while (1) {
		if (!da9061_bsp_i2c_probe(slave)) {
			if (da9061_bsp_i2c_read((slave+1), 0x81, &value)) {
				printf("Read DA9061 device ID error!\n");
				ret = -1;
			} else {
				if (DA9061_PMIC_CHIP_ID == value) {
					printf
					("DA9061 found! deviceid=%x\n", value);
					da9061_check_fault_log();
					return 0;
				} else {
					DLG_DBG
					("Device found a deviceid=%x\n", value);
					return -1;
				}
			}
		} else {
			err_count++;
			DLG_DBG
			("No device: DA9061 not found (again %d)\n", err_count);
			udelay(1000000);
		}

		if (err_count >= retry_count) {
			DLG_DBG
			("No device at 0x%x: DA9061 not found!\n", slave);
			return -1;
		}
	}

	return 0;
}


/*
 * Watchdog selector to timeout in seconds.
 *   0: WDT disabled;
 *   others: timeout = 2048 ms * 2^(TWDSCALE-1).
 */
static const unsigned int wdt_timeout[] = { 0, 2, 4, 8, 16, 32, 65, 131 };
#define DA9061_TWDSCALE_DISABLE		0
#define DA9061_TWDSCALE_MIN		1
#define DA9061_TWDSCALE_MAX		(ARRAY_SIZE(wdt_timeout) - 1)
#define DA9061_WDT_MIN_TIMEOUT		wdt_timeout[DA9061_TWDSCALE_MIN]
#define DA9061_WDT_MAX_TIMEOUT		wdt_timeout[DA9061_TWDSCALE_MAX]
#define DA9061_WDG_TIMEOUT			wdt_timeout[3]

/* got from CONTROL_D reg in OTP */
#define DA9061_WDT_DEFAULT_TIMEOUT	2

static unsigned int _da9061_wdt_timeout_to_sel(unsigned int secs)
{
	unsigned int i;

	for (i = DA9061_TWDSCALE_MIN; i <= DA9061_TWDSCALE_MAX; i++) {
		if (wdt_timeout[i] >= secs)
			return i;
	}

	return DA9061_TWDSCALE_MAX;
}

static int _da9061_wdt_set_timeout(unsigned int regval)
{
	return da9061_update_bits(DA9061_REG_CONTROL_D,
				DA9061_TWDSCALE, regval);
}


static int da9061_wdt_start(unsigned int time)
{
	unsigned int selector;
	int ret;

	DLG_DBG("[DA9061 WDT] timer start (time = %d)\n", time);

	selector = _da9061_wdt_timeout_to_sel(time);
	ret = _da9061_wdt_set_timeout(selector);
	if (ret)
		printf("[DA9061 WDT] failed to start (err = %d)\n", ret);

	return ret;
}


void hw_watchdog_reset(void)
{
	int ret;

	if (!connected)
		return;

	if (da9061_check_device_ID()) {
		connected = false;
		return;
	}

	DLG_DBG("[DA9061 WDT] watchdog time reset\n");

	ret = da9061_reg_write(DA9061_REG_CONTROL_F, DA9061_WATCHDOG);
	if (ret)
		printf("\t[DA9061 WDT] Failed to reset (err = %d)\n", ret);

	return ret;
}

static int da9061_wdt_disable(void)
{
	int ret;

	DLG_DBG("[DA9061 WDT] watchdog timer disable\n");

	ret = da9061_update_bits(DA9061_REG_CONTROL_D, DA9061_TWDSCALE,
					DA9061_TWDSCALE_DISABLE);
	if (ret)
		printf("[DA9061 WDT] failed to stop (err = %d)\n", ret);

	return ret;
}


/* watchdog timer initialize and start */
void hw_watchdog_init(void)
{
#ifdef SUPPORT_WATCHDOG_BOOTM
	char *s;
	unsigned int wtime = DA9061_WDT_DEFAULT_TIMEOUT;
	int timeout = DA9061_WDT_DEF_TIMEOUT;

	s = getenv("watchdog");

	if (s == NULL)
		return -1;

	if (strncmp(s, "on", 2) == 0) {
		printf("[DA9061 WDT] watchdog -> on\n");
		/* in case of DA9061, no need to start in here.
			call da9061_wdt_reset()
		*/
		s = getenv("watchdogtime");

		if (s != NULL)
			wtime = (unsigned int)simple_strtoul(s, NULL, 10);

		printf("[DA9061 WDT] watchdogtime is (%d)\n", wtime);
		if (wtime > DA9061_TWDSCALE_MAX) {
			printf
			("[DA9061] invalid time, set default (%d)\n", timeout);
			wtime = DA9061_WDT_DEFAULT_TIMEOUT;
		}
		da9061_wdt_start(wtime);

	} else if (strncmp(s, "off", 3) == 0) {
		printf("[DA9061 WDT] watchdog -> off\n");
		da9061_wdt_disable();
	} else {
		printf("[DA9061 WDT] watchdog -> no action\n");
	}
#else
	DLG_DBG("[%s]\n", __func__);

	hw_watchdog_reset();
#endif
	return 0;
}

