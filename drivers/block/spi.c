/*
 * SPI block device support
 *
 * Copyright (c) 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <config.h>
#include <common.h>
#include <part.h>
#include <spi_flash.h>
#include <os.h>
#include <malloc.h>
#include <asm/errno.h>

static block_dev_desc_t spi_block_dev;

static unsigned long spi_block_read(int dev, lbaint_t start,
				     lbaint_t blkcnt, void *buffer)
{
	struct spi_flash *flash;
	flash = (struct spi_flash *)spi_block_dev.priv;
	int ret;

	ret = spi_flash_read(flash, (u32)(start * spi_block_dev.blksz),
			(u32)(blkcnt * spi_block_dev.blksz), (char *)buffer);

	return (ret == 0) ? blkcnt : ret;
}

static unsigned long spi_block_write(int dev, lbaint_t start,
				      lbaint_t blkcnt, const void *buffer)
{
	struct spi_flash *flash;
	flash = (struct spi_flash *)spi_block_dev.priv;
	int ret;

	ret = spi_flash_write(flash, (start * spi_block_dev.blksz),
			(blkcnt * spi_block_dev.blksz), (char *)buffer);

	return (ret == 0) ? blkcnt : ret;
}

static unsigned long spi_block_erase(int dev, lbaint_t start, lbaint_t blkcnt)
{
	struct spi_flash *flash;
	flash = (struct spi_flash *)spi_block_dev.priv;
	int ret;

	ret = spi_flash_erase(flash, start * spi_block_dev.blksz,
			blkcnt * spi_block_dev.blksz);

	return (ret == 0) ? blkcnt : ret;
}
#ifdef CONFIG_PARTITIONS
block_dev_desc_t *spi_get_dev(int dev)
{
#if defined(CONFIG_SPI_FLASH)
	struct spi_flash *flash;
	flash = spi_flash_probe(CONFIG_SF_DEFAULT_BUS, CONFIG_SF_DEFAULT_CS,
				CONFIG_SF_DEFAULT_SPEED,
				CONFIG_SF_DEFAULT_MODE);
	if (!flash) {
		printf("There is no SPI NOR inserted!\n");
		return NULL;
	}
	if (spi_block_dev.blksz == 0) {
		spi_block_dev.type = DEV_TYPE_HARDDISK;
		spi_block_dev.blksz = 512;
		spi_block_dev.log2blksz = LOG2(512);
		spi_block_dev.dev = dev;
		spi_block_dev.part_type = PART_TYPE_UNKNOWN;
		spi_block_dev.if_type = IF_TYPE_SATA;
		spi_block_dev.lba = flash->size / 512;
		spi_block_dev.removable = 1;
		spi_block_dev.block_read = spi_block_read;
		spi_block_dev.block_write = spi_block_write;
		spi_block_dev.block_erase = spi_block_erase;
		spi_block_dev.priv = flash;
	}
#endif
	return &spi_block_dev;
}
#endif
