/*
 * Nanddisk firmware driver for CSR SiRFatlasVI
 *
 * Copyright (c) 2012 Cambridge Silicon Radio Limited, a CSR plc group company.
 *
 * Licensed under GPLv2 or later.
 */

#include <common.h>
#include <malloc.h>
#include <asm/arch/nand.h>
#include <asm/arch/rst.h>
#include <asm/arch/rsc.h>
#include <asm/arch/gpio.h>
#include <asm/arch/clk.h>
#include <asm/arch/timer.h>
#include <asm/arch/uart.h>
#include <asm/arch/efuse.h>

#if defined(CONFIG_ARCH_ATLAS7)
#include <asm/arch/ioctop.h>
#endif

#include "nanddisk.h"
#include <part.h>
#include <asm/io.h>
#include <linux/err.h>
#include <spi_flash.h>

#define NANDDISK_PRINTF(fmt, args...)	\
	debug_cond(CONFIG_NANDDISK_DEBUG, fmt, ##args)

#if defined(CONFIG_ARCH_PRIMAII)
static int nanddisk_resouce[] = {
	OST_MODULE_BASE,
	RESET_CONTROLLER_BASE,
	CLK_CTRL_BASE,
	RSC_BASE,
	GPIO_BASE,
	NAND_BASE,
	DMAC0_BASE,
	INT_BASE,
	UART1_MODULE_BASE,
	EFUSE_BASE,
	CONFIG_SYS_PL310_BASE
};
#elif defined(CONFIG_ARCH_ATLASVI)
static int nanddisk_resouce[] = {
	OST_MODULE_BASE,
	RESET_CONTROLLER_BASE,
	CLK_CTRL_BASE,
	RSC_BASE,
	GPIO_BASE,
	NAND_BASE,
	DMAC0_BASE,
	INT_BASE,
	UART1_MODULE_BASE,
	EFUSE_BASE
};
#elif defined(CONFIG_ARCH_ATLAS7)

static int nanddisk_resouce[] = {
	OST_MODULE_BASE,
	CLK_CTRL_BASE,
	IOC_TOP_BASE,
	NAND_BASE,
	INT_BASE,
	UART1_MODULE_BASE
};
#endif

static block_dev_desc_t nanddisk_dev_desc;

#define BOOT_ZONE  0
#define FTL_ZONE   1
#define TOTAL_ZONE 8

#define UBOOT_UPDATE_BUF  0x01000000
#define UBOOT_MAX_LENGTH  0x80000
#define ZONE_MAGIC_BASE   0xabcd6700

/*
 * param area in uboot is 2K
*/
#define BOOT_IMAGE_SECURE_HEADER_SIZE 0x800

/*
 * linux can support max 4k sector size block device
*/
#define LINUX_MAX_SECTOR_SIZE_SHIFT   12

static unsigned CHIP_ID;

static unsigned boot_zone_size;

static struct NAND_CHIP_INFO nand_chip_info;
static unsigned zone_handle[TOTAL_ZONE];
static struct NAND_ZONE nand_zone[TOTAL_ZONE];
static unsigned version;

static unsigned boot_zone_log_sector_start, boot_zone_log_sector_num;
static PFN_NANDDISK_IOCTRL nanddisk_ioctrl
	= (PFN_NANDDISK_IOCTRL)CONFIG_NANDDISK_ENTRY_VADDR;

static int nanddisk_io(unsigned sector, unsigned nsect, const void *buffer,
		int write)
{
	struct NAND_IO nand_io;

	if (sector + nsect > nanddisk_dev_desc.lba) {
		printf("nand: Beyond-end %s (%d %d), max %ld\n",
		       write ? "write" : "read",
		       sector, nsect,
		       nanddisk_dev_desc.lba);
		return -EINVAL;
	}

	nand_io.start_sector = sector;
	nand_io.sector_num = nsect;
	nand_io.sector_buf = (void *)buffer;

	if (!nanddisk_ioctrl(MAP_HANDLE, write ?
				NAND_IOCTRL_WRITE_SECTOR :
				NAND_IOCTRL_READ_SECTOR,
				&nand_io, sizeof(nand_io),
				NULL, 0, NULL)) {
		printf("%s:nanddisk_ioctrl(%s,%d,%d,0x%p) "
				"failed.\n", __func__, write ?
				"NAND_IOCTRL_WRITE_SECTOR" :
				"NAND_IOCTRL_READ_SECTOR", sector,
				nsect, buffer);
		return -EIO;
	}

	return 0;
}

unsigned long nanddisk_read(int dev, unsigned long start, lbaint_t blkcnt,
		void *buffer)
{
	if (blkcnt == 0)
		return 0;

	if (nanddisk_io(start, blkcnt, buffer, 0)) {
		printf("nand read failed\n");
		return -EIO;
	}

	return blkcnt;
}

unsigned long nanddisk_write(int dev, unsigned long start, lbaint_t blkcnt,
		const void *buffer)
{
	if (blkcnt == 0)
		return 0;

	if (nanddisk_io(start, blkcnt, buffer, 1)) {
		printf("nand write failed!\n");
		return -EIO;
	}

	return blkcnt;
}

static int nand_reset(void)
{
	unsigned signatrue = NAND_RESET_CHIP_SIGNATURE;
	NANDDISK_PRINTF("resetting nand chip ...\n");
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_RESET_CHIP,
			&signatrue, sizeof(signatrue), NULL, 0, NULL)) {
		return -EIO;
	}
	NANDDISK_PRINTF("reset done.\n");
	return 0;
}

/* check the chip is burned by programmer */
#define PROG_BLK_MAX 100
static int nand_format(void)
{
	unsigned flag;
	unsigned prog_blk_max, prog_mode;
	struct CREATE_ZONE create_zone;

	/* delete all existed zones */
	if (nand_reset()) {
		printf("nand_reset() failed.\n");
		return -EIO;
	}

	prog_blk_max = PROG_BLK_MAX;
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_CHIP_IS_PROG,
			     &prog_blk_max, sizeof(unsigned),
			     &prog_mode, sizeof(unsigned), NULL)) {
		printf("check prog mode failed.\n");
		return -EIO;
	}

	/* set chip mode again after reset nand */
	if (nand_chip_info.chip_feature.plane_num_bits ||
			nand_chip_info.chip_feature.interleave_num_bits)
		flag = CHIP_FLAG(CHIP_FLAG_PARALLEL_MODE,
				LINUX_MAX_SECTOR_SIZE_SHIFT);
	else
		flag = CHIP_FLAG(0, LINUX_MAX_SECTOR_SIZE_SHIFT);

	NANDDISK_PRINTF("set chip mode 0x%x\n", flag);
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_SET_CHIPMODE, &flag,
			sizeof(unsigned), NULL, 0, NULL)) {
		printf("struct NAND_IOCTRL_SET_CHIPMODE failed.\n");
		return -EIO;
	}

	NANDDISK_PRINTF("get chip info after set mode.\n");
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_GET_CHIPINFO, NULL, 0,
			&nand_chip_info, sizeof(struct NAND_CHIP_INFO), NULL)) {
		printf("struct NAND_IOCTRL_GET_CHIPINFO failed.\n");
		return -EIO;
	}

	NANDDISK_PRINTF("active chip info:\n");
	NANDDISK_PRINTF("  actived=0x%x\n", nand_chip_info.actived);
	NANDDISK_PRINTF("  chip_mode=0x%x\n", nand_chip_info.chip_mode);
	NANDDISK_PRINTF("  active blk info:\n");
	NANDDISK_PRINTF("    block_num=%d\n",
			nand_chip_info.active_bdev_info.block_num);
	NANDDISK_PRINTF("    sector_per_block=%d\n",
			nand_chip_info.active_bdev_info.sector_per_block);
	NANDDISK_PRINTF("    byte_per_sector=%d\n",
			nand_chip_info.active_bdev_info.byte_per_sector);
	NANDDISK_PRINTF("  io blk info:\n");
	NANDDISK_PRINTF("    block_num=%d\n",
			nand_chip_info.io_bdev_info.block_num);
	NANDDISK_PRINTF("    sector_per_block=%d\n",
			nand_chip_info.io_bdev_info.sector_per_block);
	NANDDISK_PRINTF("    byte_per_sector=%d\n",
			nand_chip_info.io_bdev_info.byte_per_sector);

	memset(&create_zone, 0, sizeof(create_zone));

	/* create boot zone */
	create_zone.start_block = 0;
	create_zone.log_block_num = 0;
	create_zone.size = boot_zone_size / NAND_SECTOR_SIZE_UNIT;
	create_zone.block_flag = 0;
	create_zone.block_flag |= nand_chip_info.chip_feature.randmizer ?
					BLOCK_FLAG_RANDMIZER : 0;
	create_zone.zone_flag = ZONE_FLAG_GUARANTEE_USABLE_SIZE;
	if (prog_mode)
		create_zone.zone_flag |= ZONE_FLAG_SKIP_FORMAT;
	create_zone.type = BOOT_ZONE_TYPE;
	create_zone.magic = ZONE_MAGIC_BASE;
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_CREATE_ZONE, &create_zone,
			sizeof(struct CREATE_ZONE), &zone_handle[BOOT_ZONE],
			sizeof(unsigned), NULL)) {
		printf("boot zone create failed.\n");
		return -EIO;
	}

	if (!nanddisk_ioctrl(zone_handle[BOOT_ZONE], NAND_IOCTRL_GET_ZONEINFO,
			NULL, 0, &nand_zone[BOOT_ZONE],
			sizeof(struct NAND_ZONE), NULL)) {
		printf("failed to get bootzone info.\n");
		return -EIO;
	}

	/* create ftl zone */
	create_zone.start_block = nand_zone->active_block_num;
	create_zone.log_block_num = 0;
	create_zone.size = nand_chip_info.active_bdev_info.block_num -
				nand_zone->active_block_num;
	create_zone.block_flag =
		nand_chip_info.chip_mode & CHIP_FLAG_PARALLEL_MODE ?
			BLOCK_FLAG_PARALLEL_MODE : 0;
	create_zone.block_flag |=
			nand_chip_info.chip_feature.randmizer ?
			BLOCK_FLAG_RANDMIZER : 0;
	create_zone.zone_flag = 0;
	if (prog_mode)
		create_zone.zone_flag |= ZONE_FLAG_SKIP_FORMAT;
	create_zone.type = FTL_ZONE_TYPE;
	create_zone.magic = ZONE_MAGIC_BASE+1;
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_CREATE_ZONE, &create_zone,
			sizeof(struct CREATE_ZONE), &zone_handle[FTL_ZONE],
			sizeof(unsigned), NULL)) {
		printf("ftl zone create failed.\n");
		return -EIO;
	}

	if (!nanddisk_ioctrl(zone_handle[FTL_ZONE], NAND_IOCTRL_GET_ZONEINFO,
			NULL, 0, &nand_zone[FTL_ZONE],
			sizeof(struct NAND_ZONE), NULL)) {
		printf("failed to get ftl info.\n");
		return -EIO;
	}

	return 0;
}

int nanddisk_init(void)
{
	unsigned  enable;
	unsigned flag;
	unsigned zone;
	struct ADDRMAP *addr_map_nanddisk;
	struct ADDRMAP nanddisk_map_tbl[30];
	unsigned i;
	unsigned valid_zones = 1;
	struct ZONE_MAP zone_map;
	unsigned map_size;
	unsigned space_size;

	CHIP_ID = readl(CHIP_ID_PA);;

	NANDDISK_PRINTF("nanddisk_bin_start 0x%x, nanddisk_bin_end 0x%x\n",
		(unsigned)&nanddisk_bin_start,
		(unsigned)&nanddisk_bin_end);

	if (CONFIG_NANDDISK_CODE_SIZE < (unsigned)&nanddisk_bin_end -
			(unsigned)&nanddisk_bin_start) {
		printf("rsv size(0x%x) for nanddisk is not enough\n",
		       CONFIG_NANDDISK_CODE_SIZE);
		return -EINVAL;
	}

	/* copy nanddisk code to its run work address */
	NANDDISK_PRINTF("copy nanddisk binary to address 0x%x, size 0x%x\n",
		CONFIG_NANDDISK_ENTRY_VADDR,
		(unsigned)&nanddisk_bin_end - (unsigned)&nanddisk_bin_start);

	memcpy((void *)CONFIG_NANDDISK_ENTRY_VADDR, (void *)&nanddisk_bin_start,
	       (unsigned)&nanddisk_bin_end - (unsigned)&nanddisk_bin_start);

	flush_dcache_all();
	invalidate_icache_all();

	NANDDISK_PRINTF("copy nanddisk binary finish!\n");

	space_size = CONFIG_NANDDISK_CODE_SIZE;
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_RUNTIME_INIT, &space_size,
			sizeof(unsigned), NULL, 0, NULL)) {
		printf("struct NAND_IOCTRL_RUNTIME_INIT failed.\n");
		return -EIO;
	}

	/* check the interface version */
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_GET_INTERFACE_VERSION,
			NULL, 0, &version, sizeof(unsigned), NULL)) {
		printf("get interface version failed.\n");
		return -EIO;
	}

	NANDDISK_PRINTF("nanddisk binary's interface version is %d\n", version);
	if (NANDDISK_INTERFACE_VERSION != version) {
		printf("interface ver mismatch (using version %d), "
			"please update your nanddisk.h file.",
			NANDDISK_INTERFACE_VERSION);
		return -EIO;
	}

	/* get binary version */
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_GET_VERSION, NULL, 0,
			&version, sizeof(unsigned), NULL)) {
		printf("NAND_IOCTRL_GET_VERSION failed.\n");
		return -EIO;
	}
	NANDDISK_PRINTF("nanddisk binary version is 0x%x\n", version);


	/* tell nanddisk the address map */
	memset(nanddisk_map_tbl, 0, sizeof(nanddisk_map_tbl));
	addr_map_nanddisk = nanddisk_map_tbl;
	i = 0;
	while (i < ARRAY_SIZE(nanddisk_resouce)) {
		addr_map_nanddisk->flag = 0;
		addr_map_nanddisk->pa = nanddisk_resouce[i];
		addr_map_nanddisk->va = (void *)nanddisk_resouce[i];
		addr_map_nanddisk->size = 0x10000;
		NANDDISK_PRINTF("va:0x%p, pa:0x%x, size:0x%x\n",
				addr_map_nanddisk->va,
				addr_map_nanddisk->pa,
				addr_map_nanddisk->size);
		addr_map_nanddisk++;
		i++;
	}

	addr_map_nanddisk->flag = ADDR_MAP_FLAG_CACHE;
	addr_map_nanddisk->pa = CONFIG_NANDDISK_ENTRY_PADDR;
	addr_map_nanddisk->va = (void *)CONFIG_NANDDISK_ENTRY_VADDR;
	addr_map_nanddisk->size = CONFIG_NANDDISK_CODE_SIZE;
	NANDDISK_PRINTF("fw paddr:0x%p, vaddr:0x%x, size:0x%x\n",
			addr_map_nanddisk->va,
			addr_map_nanddisk->pa,
			addr_map_nanddisk->size);

	addr_map_nanddisk++;

	addr_map_nanddisk->flag = ADDR_MAP_FLAG_DMABUF;
	addr_map_nanddisk->pa = CONFIG_NANDDISK_DMA_BUF_PADDR;
	addr_map_nanddisk->va = (void *)CONFIG_NANDDISK_DMA_BUF_VADDR;
	addr_map_nanddisk->size = CONFIG_NANDDISK_DMA_BUF_SIZE;
	NANDDISK_PRINTF("fw paddr:0x%p, vaddr:0x%x, size:0x%x\n",
			addr_map_nanddisk->va,
			addr_map_nanddisk->pa,
			addr_map_nanddisk->size);

	if (!nanddisk_ioctrl(0, NAND_IOCTRL_NEW_ADDRESS_MAP, nanddisk_map_tbl,
			sizeof(nanddisk_map_tbl), NULL, 0, NULL)) {
		printf("NAND_IOCTRL_NEW_ADDRESS_MAP failed.\n");
		return -EIO;
	}

	/* enable nanddisk debug message */
	enable = CONFIG_NANDDISK_DEBUG;
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_ENABLE_MSG, &enable,
			sizeof(unsigned), NULL, 0, NULL)) {
		printf("NAND_IOCTRL_ENABLE_MSG failed.\n");
		return -EIO;
	}

	/* power on the nandchip */
	enable = 1;
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_POWER, &enable,
			sizeof(unsigned), NULL, 0, NULL)) {
		printf("NAND_IOCTRL_POWER failed.\n");
		return -EIO;
	}

	/* get chip info */
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_GET_CHIPINFO, NULL,
			0, &nand_chip_info,
			sizeof(struct NAND_CHIP_INFO), NULL)) {
		printf("struct NAND_IOCTRL_GET_CHIPINFO failed.\n");
		return -EIO;
	}

	if (nand_chip_info.chip_feature.randmizer && (CHIP_ID == 0x1501)) {
		boot_zone_size = (nand_chip_info.phy_bdev_info.sector_per_block
			* nand_chip_info.phy_bdev_info.byte_per_sector) * 15;
	} else {
		boot_zone_size = (nand_chip_info.phy_bdev_info.sector_per_block
			* nand_chip_info.phy_bdev_info.byte_per_sector) * 4;
	}
	if (boot_zone_size < CONFIG_NANDDISK_BOOTZONE_SIZE) {
		printf("commit flag addr(0x%x) is out of boot zone (0x%x)\n",
		       CONFIG_NANDDISK_BOOTZONE_SIZE, boot_zone_size);
		boot_zone_size = CONFIG_NANDDISK_BOOTZONE_SIZE;
		printf("boot zone has been adapt to 0x%x\n", boot_zone_size);
	}

	/* set chip mode */
	if (nand_chip_info.chip_feature.plane_num_bits ||
			nand_chip_info.chip_feature.interleave_num_bits)
		flag = CHIP_FLAG(CHIP_FLAG_PARALLEL_MODE,
				LINUX_MAX_SECTOR_SIZE_SHIFT);
	else
		flag = CHIP_FLAG(0, LINUX_MAX_SECTOR_SIZE_SHIFT);

	if (!nanddisk_ioctrl(0, NAND_IOCTRL_SET_CHIPMODE, &flag,
			sizeof(unsigned), NULL, 0, NULL)) {
		printf("NAND_IOCTRL_SET_CHIPMODE failed.\n");
		valid_zones = 0;
	} else {
		if (!nanddisk_ioctrl(0, NAND_IOCTRL_GET_CHIPINFO, NULL,
				0, &nand_chip_info,
				sizeof(struct NAND_CHIP_INFO), NULL)) {
			printf("NAND_IOCTRL_GET_CHIPINFO failed.\n");
			return -EIO;
		}
	}

	/* check this chip has been correctly zoned. */
	if (valid_zones) {
		i = 0;
		while (nanddisk_ioctrl(0, NAND_IOCTRL_OPEN_ZONE, &i, sizeof(i),
				&zone, sizeof(zone), NULL)) {
			nanddisk_ioctrl(zone, NAND_IOCTRL_GET_ZONEINFO, NULL,
				0, nand_zone, sizeof(struct NAND_ZONE), NULL);
			if (ZONE_MAGIC_BASE + i != nand_zone->magic) {
				printf("invalid zone magic.\n");
				valid_zones = 0;
			}
			i++;
		}
		NANDDISK_PRINTF("Totaly %d zones\n", i);
	}

	/* if this chip is invalid for uboot, format it */
	if (!i || !valid_zones) {
		NANDDISK_PRINTF("formating nand ...\n");
		if (nand_format()) {
			printf("format nand failed\n");
			return -EIO;
		}
	}

	nanddisk_dev_desc.if_type = IF_TYPE_SATA;
	nanddisk_dev_desc.dev = 0;
	nanddisk_dev_desc.part_type = PART_TYPE_DOS;
	nanddisk_dev_desc.type = DEV_TYPE_HARDDISK;
	nanddisk_dev_desc.blksz = nand_chip_info.io_bdev_info.byte_per_sector;
	nanddisk_dev_desc.log2blksz = LOG2(nanddisk_dev_desc.blksz);
	sprintf(nanddisk_dev_desc.revision, "%x", version);
	nanddisk_dev_desc.block_read = nanddisk_read;
	nanddisk_dev_desc.block_write = nanddisk_write;

	/* init the boot zone boundary variables */
	boot_zone_log_sector_start = 1;
	NANDDISK_PRINTF("boot_zone_log_sector_start=%d\n",
			boot_zone_log_sector_start);
	boot_zone_log_sector_num =
		(CONFIG_NANDDISK_BOOTZONE_SIZE >> nanddisk_dev_desc.log2blksz);

	/* get all zones' handle, and mount them */
	for (i = 0; i < TOTAL_ZONE; i++) {
		if (nanddisk_ioctrl(0, NAND_IOCTRL_OPEN_ZONE, &i,
				sizeof(i), &zone_handle[i],
				sizeof(unsigned), NULL)) {
			if (nanddisk_ioctrl(zone_handle[i],
					NAND_IOCTRL_MOUNT_ZONE, NULL, 0,
					NULL, 0, NULL)) {
				nanddisk_ioctrl(zone_handle[i],
						NAND_IOCTRL_GET_ZONEINFO, NULL,
						0, &nand_zone[i],
						sizeof(struct NAND_ZONE), NULL);
			} else {
				printf("mount zone %d failed.\n", i);
				return -EIO;
			}
		} else {
			printf("open zone %d failed.\n", i);
			break;
		}
	}

	nanddisk_dev_desc.lba =
		nand_zone[FTL_ZONE].log_block_num *
		nand_zone[FTL_ZONE].io_bdev_info.sector_per_block;

	NANDDISK_PRINTF("log_block_num = %d\n",
			nand_zone[FTL_ZONE].log_block_num);
	NANDDISK_PRINTF("io_bdev_info.sector_per_block = %d\n",
			nand_zone[FTL_ZONE].io_bdev_info.sector_per_block);
	NANDDISK_PRINTF("nanddisk_dev_desc.lba = %ld\n",
			nanddisk_dev_desc.lba);
	NANDDISK_PRINTF("size of nanddisk_dev_desc.lba = %d\n",
			sizeof(nanddisk_dev_desc.lba));

	/* set zone map */
	if (!nanddisk_ioctrl(0, NAND_IOCTRL_GET_ZONEMAP_SIZE, NULL, 0,
			&map_size, sizeof(map_size), NULL)) {
		printf("struct NAND_IOCTRL_GET_ZONEMAP_SIZE failed\n");
		return -EIO;
	}
	NANDDISK_PRINTF("map size is %d\n", map_size);

	if (!map_size) {
		zone_map.priority = 5;
		zone_map.start = boot_zone_log_sector_start;
		zone_map.sector_num = boot_zone_log_sector_num;
		if (!nanddisk_ioctrl(zone_handle[BOOT_ZONE],
				NAND_IOCTRL_SET_ZONEMAP, &zone_map,
				sizeof(zone_map), NULL, 0, NULL)) {
			printf("NAND_IOCTRL_SET_ZONEMAP failed.\n");
			return -EIO;
		}

		zone_map.priority = 4;
		zone_map.start = 0;
		zone_map.sector_num = nand_zone[FTL_ZONE].log_block_num *
			nand_zone[FTL_ZONE].io_bdev_info.sector_per_block;
		if (!nanddisk_ioctrl(zone_handle[FTL_ZONE],
				NAND_IOCTRL_SET_ZONEMAP, &zone_map,
				sizeof(zone_map), NULL, 0, NULL)) {
			printf("NAND_IOCTRL_SET_ZONEMAP failed.\n");
			return -EIO;
		}
	}

	return 0;
}

int nanddisk_map_change(enum nanddisk_map_state state)
{
	struct ZONE_MAP zone_map;
	int i;

	zone_map.priority = 5;
	zone_map.start = (state == NANDISK_MAP_BOOTPARTITION ? 0 : 1);
	zone_map.sector_num = boot_zone_log_sector_num;
	if (!nanddisk_ioctrl(zone_handle[BOOT_ZONE],
			     NAND_IOCTRL_SET_ZONEMAP, &zone_map,
			     sizeof(zone_map), NULL, 0, NULL)) {
		printf("NAND_IOCTRL_SET_ZONEMAP failed.\n");
		return -EIO;
	}

	zone_map.priority = 4;
	zone_map.start = (state == NANDISK_MAP_BOOTPARTITION ?
				(zone_map.start + zone_map.sector_num) : 0);
	zone_map.sector_num = nand_zone[FTL_ZONE].log_block_num *
		nand_zone[FTL_ZONE].io_bdev_info.sector_per_block;
	if (!nanddisk_ioctrl(zone_handle[FTL_ZONE],
			     NAND_IOCTRL_SET_ZONEMAP, &zone_map,
			     sizeof(zone_map), NULL, 0, NULL)) {
		printf("NAND_IOCTRL_SET_ZONEMAP failed.\n");
		return -EIO;
	}

	for (i = 0; i < TOTAL_ZONE; i++) {
		if (nanddisk_ioctrl(0, NAND_IOCTRL_OPEN_ZONE, &i,
				    sizeof(i), &zone_handle[i],
				    sizeof(unsigned), NULL)) {
			nanddisk_ioctrl(zone_handle[i],
					NAND_IOCTRL_GET_ZONEINFO, NULL,
					0, &nand_zone[i],
					sizeof(struct NAND_ZONE), NULL);
		} else {
			break;
		}
	}

	return 0;
}

void nanddisk_print_info(void)
{
	struct BLK_DEV_INFO *bdev_info = NULL;
	struct ZONE_MAP *zone_map = NULL;
	u64 size;
	int i;

	printf("\n== nanddisk info  ==\n");
	printf("<binary info>\t0x%x\n", version);

	printf("\n== flash info ==\n");
	bdev_info = &nand_chip_info.phy_bdev_info;
	printf("<physical info, refer to spec>\n");
	printf("\t<page size>\t%d\n", bdev_info->byte_per_sector);
	printf("\t<page in blk>\t%d\n", bdev_info->sector_per_block);
	printf("\t<blk num>\t%d\n", bdev_info->block_num);
	size = (u64)bdev_info->byte_per_sector *
		bdev_info->sector_per_block;
	printf("\t<blk size>\t");
	print_size(size, "\n");
	size = size * bdev_info->block_num;
	printf("\t<flash size>\t");
	print_size(size, "\n");

	printf("\n== zone info ==\n");
	for (i = 0; i < TOTAL_ZONE; i++) {
		bdev_info = &nand_zone[i].bdev_info;

		if (!nand_zone[i].type)
			break;

		printf("<zone type %d>\n", nand_zone[i].type);
		printf("\t<physical info>\n");
		printf("\t\t<page size>\t%d\n", bdev_info->byte_per_sector);
		printf("\t\t<page in blk>\t%d\n", bdev_info->sector_per_block);
		printf("\t\t<blk num>\t%d\n", bdev_info->block_num);
		size = (u64)bdev_info->byte_per_sector *
			bdev_info->sector_per_block;
		printf("\t\t<blk size>\t");
		print_size(size, "\n");
		size = size * bdev_info->block_num;
		printf("\t\t<zone size>\t");
		print_size(size, "\n");

		bdev_info = &nand_zone[i].io_bdev_info;
		printf("\t<logical info>\n");
		printf("\t\t<page size>\t%d\n", bdev_info->byte_per_sector);
		printf("\t\t<page in blk>\t%d\n", bdev_info->sector_per_block);
		printf("\t\t<blk num>\t%d\n", nand_zone[i].log_block_num);
		size = (u64)bdev_info->byte_per_sector *
			bdev_info->sector_per_block;
		printf("\t\t<blk size>\t");
		print_size(size, "\n");
		size = size * nand_zone[i].log_block_num;
		printf("\t\t<zone size>\t");
		print_size(size, "\n");

		zone_map = &nand_zone[i].zone_map;
		printf("\t<mapping info>\n");
		printf("\t\t<start page>\t%d\n", zone_map->start);
		printf("\t\t<page num>\t%d\n", zone_map->sector_num);
	}
}


#ifdef CONFIG_PARTITIONS
block_dev_desc_t *nanddisk_get_dev(int dev)
{
	return &nanddisk_dev_desc;
}
#endif

int nanddisk_erase(void)
{
	struct NANDDBG_BLOCK_INFO dbg_block;
	unsigned int block_num, i;

	block_num = nand_chip_info.phy_bdev_info.block_num;

	for (i = 0; i < block_num; i++) {
		dbg_block.block_index = i;
		dbg_block.block_flag = BLOCK_FLAG_PARALLEL_MODE_RAW;
		if (!nanddisk_ioctrl(0, NAND_IOCTRL_DBG_ERASE_BLOCK,
				     &dbg_block, sizeof(dbg_block),
				     NULL, 0, NULL))
			printf("block %d may be bad block!\n", i);
	}

	return 0;
}

int nanddisk_reset(void)
{
	if (nand_reset()) {
		printf("nand_reset() failed.\n");
		return -EIO;
	} else {
		printf("reset the device, or run 'ndisk init'.\n");
	}

	return 0;
}

