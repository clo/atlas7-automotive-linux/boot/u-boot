#include <asm/io.h>
#include <asm/arch/board.h>
#include <asm/arch/pwm.h>
#include <common.h>

#define SIRF_PWM_BASE					0x18630000

#define SIRF_PWM_SELECT_PRECLK			0x0
#define SIRF_PWM_OE						0x4
#define SIRF_PWM_ENABLE_PRECLOCK		0x8
#define SIRF_PWM_ENABLE_POSTCLOCK		0xC
#define SIRF_PWM_GET_WAIT_OFFSET(n)		(0x10 + 0x8*n)
#define SIRF_PWM_GET_HOLD_OFFSET(n)		(0x14 + 0x8*n)

#define SIRF_PWM_TR_STEP(n)				(0x48 + 0x8*n)
#define SIRF_PWM_STEP_HOLD(n)			(0x4c + 0x8*n)

#define SRC_FIELD_SIZE					3
#define BYPASS_MODE_BIT					21
#define TRANS_MODE_SELECT_BIT			7

#define SIRF_MAX_SRC_CLK				5

enum {
	CLK_XIN,
	CLK_SYS0_PLL,
	CLK_SYS1_PLL,
	CLK_XINW,
	CLK_SYS2_PLL,
};

/* input clock frequency table , hz*/
static u32 clock_table[SIRF_MAX_SRC_CLK] = {
	26000000,
	1040000000,
	1200001144,
	32768,
	1625000000,
};

static u32 pwm_ns_to_cycles(struct pwm_device *pwm, int time_ns)
{
	u32 src_clk_rate = clock_table[pwm->sigsrc_clk_idx];
	u32 cycles;

#ifndef USEC_PER_SEC
# define USEC_PER_SEC			1000000UL
#endif

	cycles = ((src_clk_rate/USEC_PER_SEC) * time_ns)/1000;

	return cycles;
}

void sirf_pwm_config(struct pwm_device *pwm, int duty_ns, int period_ns)
{
	u32 cycles, high_cycles, low_cycles;

	pwm->duty_ns = duty_ns;
	pwm->period_ns = period_ns;
	pwm->sigsrc_clk_idx = CLK_SYS0_PLL;

	cycles = pwm_ns_to_cycles(pwm, pwm->period_ns);

	/*
	 * enter bypass mode, high_cycles and low_cycle
	 * do not need to config if period_cycles == 1
	 */
	if (cycles <= 1) {
		pwm->bypass_mode = 1;
	} else {
		pwm->bypass_mode = 0;

		high_cycles = pwm_ns_to_cycles(pwm, pwm->duty_ns);
		low_cycles = cycles - high_cycles;

		/*
		 * high_cycles will equal to period_cycles when duty_ns
		 * is big enough, so low_cycles will be 0,
		 * a wrong value will be written to register after
		 * low_cycles minus 1 later.
		 */
		if (high_cycles == cycles) {
			high_cycles--;
			low_cycles = 1;
		}
		writel(low_cycles,
		       SIRF_PWM_BASE + SIRF_PWM_GET_HOLD_OFFSET(pwm->pwm));
		writel(high_cycles,
		       SIRF_PWM_BASE + SIRF_PWM_GET_WAIT_OFFSET(pwm->pwm));
		/* use gradually switch mode */
		writel(2, SIRF_PWM_BASE + SIRF_PWM_TR_STEP(pwm->pwm));
		writel(cycles, SIRF_PWM_BASE + SIRF_PWM_STEP_HOLD(pwm->pwm));
	}
}

void sirf_pwm_enable(struct pwm_device *pwm)
{
	u32 val;

	/* disable preclock */
	val = readl(SIRF_PWM_BASE + SIRF_PWM_ENABLE_PRECLOCK);
	val &= ~(1 << pwm->pwm);
	writel(val, SIRF_PWM_BASE + SIRF_PWM_ENABLE_PRECLOCK);

	/* select preclock source must after disable preclk */
	val = readl(SIRF_PWM_BASE + SIRF_PWM_SELECT_PRECLK);
	val &= ~(0x7 << (SRC_FIELD_SIZE * pwm->pwm));
	val |= (pwm->sigsrc_clk_idx << (SRC_FIELD_SIZE * pwm->pwm));

	if (pwm->bypass_mode == 1)
		val |= (0x1 << (BYPASS_MODE_BIT + pwm->pwm));
	else
		val &= ~(0x1 << (BYPASS_MODE_BIT + pwm->pwm));

	writel(val, SIRF_PWM_BASE + SIRF_PWM_SELECT_PRECLK);


	/* wait for some time */
	udelay(100);

	/* enable preclock */
	val = readl(SIRF_PWM_BASE + SIRF_PWM_ENABLE_PRECLOCK);
	val |= (1 << pwm->pwm);
	writel(val, SIRF_PWM_BASE + SIRF_PWM_ENABLE_PRECLOCK);

	/* enable post clock*/
	val = readl(SIRF_PWM_BASE + SIRF_PWM_ENABLE_POSTCLOCK);
	val |= (1 << pwm->pwm);
	writel(val, SIRF_PWM_BASE + SIRF_PWM_ENABLE_POSTCLOCK);

	/* enable output, and use gradually switch mode */
	val = readl(SIRF_PWM_BASE + SIRF_PWM_OE);
	val |= 1 << pwm->pwm;

	writel(val, SIRF_PWM_BASE + SIRF_PWM_OE);
}

void sirf_pwm_disable(struct pwm_device *pwm)
{
	u32 val;

	/* disable output */
	val = readl(SIRF_PWM_BASE + SIRF_PWM_OE);
	val &= ~(1 << pwm->pwm);
	writel(val, SIRF_PWM_BASE + SIRF_PWM_OE);

	/* disable postclock */
	val = readl(SIRF_PWM_BASE + SIRF_PWM_ENABLE_POSTCLOCK);
	val &= ~(1 << pwm->pwm);
	writel(val, SIRF_PWM_BASE + SIRF_PWM_ENABLE_POSTCLOCK);

	/* disable preclock */
	val = readl(SIRF_PWM_BASE + SIRF_PWM_ENABLE_PRECLOCK);
	val &= ~(1 << pwm->pwm);
	writel(val, SIRF_PWM_BASE + SIRF_PWM_ENABLE_PRECLOCK);
}


void sirf_pwm_io_clock_enable(void)
{
#define CLKC_PWM_LEAF_CLK_EN_SET 0x1862024C
#define CLKC_PWM_LEAF_CLK_EN_CLR 0x18620250
	writel(7, CLKC_PWM_LEAF_CLK_EN_CLR);
	writel(4, CLKC_PWM_LEAF_CLK_EN_SET);
}
void sirf_gpio_mux_to_pwm(void)
{
	unsigned int val;
#define SW_TOP_FUNC_SEL_16_REG_SET 0x10e40100
#define SW_TOP_FUNC_SEL_16_REG_CLR 0x10e40104

#define GPIO2_BITS (2*4)
#define GPIO4_BITS (4*4)

	val = ((7 << GPIO2_BITS) | (7 << GPIO4_BITS));
	writel(val, SW_TOP_FUNC_SEL_16_REG_CLR);

	val = readl(SW_TOP_FUNC_SEL_16_REG_SET);
	val |= ((3 << GPIO2_BITS) | (3 << GPIO4_BITS));
	writel(val, SW_TOP_FUNC_SEL_16_REG_SET);
}


