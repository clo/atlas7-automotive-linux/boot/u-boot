/*
 * CSR sirfsoc LCD Controller driver
 *
 * Copyright (c) 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/gpio.h>

#include "sirf_lcd.h"

#define HALF_DUTY (1 << 12)

struct vidinfo panel_info;
struct vidinfo lcd_panels[] = {
	{
		.mode = {
			.hactive	= 800,
			.vactive	= 480,
			.hfront_porch	= 20,
			.hback_porch	= 234,
			.vfront_porch	= 3,
			.vback_porch	= 41,
			.hsync_len	= 3,
			.vsync_len	= 2,
			.pixclock	= 33264000,
		},
		.bpp = LCD_DEFAULT_BPP,
		.timing = 0x88,
		.vcc = 0x000d0008,
		.vdd = 0x000b0006,
		.vee = 0x000a0005,
	},
	{
		.mode = {
			.hactive	= 1024,
			.vactive	= 600,
			.hfront_porch	= 28,
			.hback_porch	= 288,
			.vfront_porch	= 8,
			.vback_porch	= 24,
			.hsync_len	= 4,
			.vsync_len	= 3,
			.pixclock	= 51200000,
		},
		.bpp = LCD_DEFAULT_BPP,
		.timing = 0xA0,
		.vcc = 0x000d0008,
		.vdd = 0x000b0006,
		.vee = 0x000a0005,
	}
};

#ifdef DUMP_REG
#define DBG_MSG(fmt, args...) printf(fmt, ##args)
#else
#define DBG_MSG(fmt, args...)
#endif

#define FB_BYTES(bpp)		(((bpp) == 24) ? 4 : ((bpp) / 8))

static void lcd_layer_reg_dump(int layer)
{
	DBG_MSG("Layer%d registers:\n", layer);
	DBG_MSG("LCD_CTRL: 0x%x\n",
		readl(panel_info.base + LCD_CTRL(layer)));
	DBG_MSG("LCD_HSTART: 0x%x\n",
		readl(panel_info.base + LCD_HSTART(layer)));
	DBG_MSG("LCD_VSTART: 0x%x\n",
		readl(panel_info.base + LCD_VSTART(layer)));
	DBG_MSG("LCD_HEND: 0x%x\n",
		readl(panel_info.base + LCD_HEND(layer)));
	DBG_MSG("LCD_VEND: 0x%x\n",
		readl(panel_info.base + LCD_VEND(layer)));
	DBG_MSG("LCD_BASE0: 0x%x\n",
		readl(panel_info.base + LCD_BASE0(layer)));
	DBG_MSG("LCD_BASE1: 0x%x\n",
		readl(panel_info.base + LCD_BASE1(layer)));
	DBG_MSG("LCD_XSIZE: 0x%x\n",
		readl(panel_info.base + LCD_XSIZE(layer)));
	DBG_MSG("LCD_YSIZE: 0x%x\n",
		readl(panel_info.base + LCD_YSIZE(layer)));
	DBG_MSG("LCD_SKIP: 0x%x\n",
		readl(panel_info.base + LCD_SKIP(layer)));
	DBG_MSG("LCD_DMA_CTRL: 0x%x\n",
		readl(panel_info.base + LCD_DMA_CTRL(layer)));
	DBG_MSG("LCD_ALPHA: 0x%x\n",
		readl(panel_info.base + LCD_ALPHA(layer)));
	DBG_MSG("LCD_CKEYB_SRC: 0x%x\n",
		readl(panel_info.base + LCD_CKEYB_SRC(layer)));
	DBG_MSG("LCD_CKEYS_SRC: 0x%x\n",
		readl(panel_info.base + LCD_CKEYS_SRC(layer)));
	DBG_MSG("LCD_FIFO_CHK: 0x%x\n",
		readl(panel_info.base + LCD_FIFO_CHK(layer)));
	DBG_MSG("LCD_FIFO_STATUS: 0x%x\n",
		readl(panel_info.base + LCD_FIFO_STATUS(layer)));
	DBG_MSG("LCD_CKEYB_DST: 0x%x\n",
		readl(panel_info.base + LCD_CKEYB_DST(layer)));
	DBG_MSG("LCD_CKEYS_DST: 0x%x\n",
		readl(panel_info.base + LCD_CKEYS_DST(layer)));

	DBG_MSG("LCD_S0_HSYNC_PERIOD: 0x%x\n",
		readl(panel_info.base + LCD_S0_HSYNC_PERIOD));
	DBG_MSG("LCD_S0_HSYNC_WIDTH: 0x%x\n",
		readl(panel_info.base + LCD_S0_HSYNC_WIDTH));
	DBG_MSG("LCD_S0_VSYNC_PERIOD: 0x%x\n",
		readl(panel_info.base + LCD_S0_VSYNC_PERIOD));
	DBG_MSG("LCD_S0_VSYNC_WIDTH: 0x%x\n",
		readl(panel_info.base + LCD_S0_VSYNC_WIDTH));
	DBG_MSG("LCD_S0_ACT_HSTART: 0x%x\n",
		readl(panel_info.base + LCD_S0_ACT_HSTART));
	DBG_MSG("LCD_S0_ACT_VSTART: 0x%x\n",
		readl(panel_info.base + LCD_S0_ACT_VSTART));
	DBG_MSG("LCD_S0_ACT_HEND: 0x%x\n",
		readl(panel_info.base + LCD_S0_ACT_HEND));
	DBG_MSG("LCD_S0_ACT_VEND: 0x%x\n",
		readl(panel_info.base + LCD_S0_ACT_VEND));
	DBG_MSG("LCD_S0_OSC_RATIO: 0x%x\n",
		readl(panel_info.base + LCD_S0_OSC_RATIO));
	DBG_MSG("LCD_S0_TIM_CTRL: 0x%x\n",
		readl(panel_info.base + LCD_S0_TIM_CTRL));
	DBG_MSG("LCD_S0_TIM_STATUS: 0x%x\n",
		readl(panel_info.base + LCD_S0_TIM_STATUS));
	DBG_MSG("LCD_S0_HCOUNT: 0x%x\n",
		readl(panel_info.base + LCD_S0_HCOUNT));
	DBG_MSG("LCD_S0_VCOUNT: 0x%x\n",
		readl(panel_info.base + LCD_S0_VCOUNT));
	DBG_MSG("LCD_S0_BLANK: 0x%x\n",
		readl(panel_info.base + LCD_S0_BLANK));
	DBG_MSG("LCD_S0_BACK_COLOR: 0x%x\n",
		readl(panel_info.base + LCD_S0_BACK_COLOR));
	DBG_MSG("LCD_S0_DISP_MODE: 0x%x\n",
		readl(panel_info.base + LCD_S0_DISP_MODE));
	DBG_MSG("LCD_S0_LAYER_SEL: 0x%x\n",
		readl(panel_info.base + LCD_S0_LAYER_SEL));
	DBG_MSG("LCD_S0_RGB_SEQ: 0x%x\n",
		readl(panel_info.base + LCD_S0_RGB_SEQ));
	DBG_MSG("LCD_S0_RGB_YUV_COEF1: 0x%x\n",
		readl(panel_info.base + LCD_S0_RGB_YUV_COEF1));
	DBG_MSG("LCD_S0_RGB_YUV_COEF2: 0x%x\n",
		readl(panel_info.base + LCD_S0_RGB_YUV_COEF2));
	DBG_MSG("LCD_S0_RGB_YUV_COEF3: 0x%x\n",
		readl(panel_info.base + LCD_S0_RGB_YUV_COEF3));
	DBG_MSG("LCD_S0_YUV_CTRL: 0x%x\n",
		readl(panel_info.base + LCD_S0_YUV_CTRL));
	DBG_MSG("LCD_S0_TV_FIELD: 0x%x\n",
		readl(panel_info.base + LCD_S0_TV_FIELD));
	DBG_MSG("LCD_S0_INT_LINE: 0x%x\n",
		readl(panel_info.base + LCD_S0_INT_LINE));
	DBG_MSG("LCD_S0_LAYER_STATUS: 0x%x\n",
		readl(panel_info.base + LCD_S0_LAYER_STATUS));
	DBG_MSG("LCD_DMA_STATUS: 0x%x\n",
		readl(panel_info.base + LCD_DMA_STATUS));
	DBG_MSG("LCD_SCR_CTRL: 0x%x\n",
		readl(panel_info.base + LCD_SCR_CTRL));
	DBG_MSG("LCD_INT_MASK: 0x%x\n",
		readl(panel_info.base + LCD_INT_MASK));
	DBG_MSG("LCD_INT_CTRL_STATUS: 0x%x\n",
		readl(panel_info.base + LCD_INT_STATUS));
}


static void lcd_config_screen(void)
{
	unsigned int tim_ctrl;
	unsigned int hsync_period, vsync_period;
	unsigned int hstart, vstart;
	unsigned int hend, vend;
	unsigned int oscratio;

	writel(0, panel_info.base + LCD_INT_MASK);
	writel(0xffff, panel_info.base + LCD_INT_STATUS);

	hsync_period = panel_info.mode.hactive +
			panel_info.mode.hsync_len +
			panel_info.mode.hfront_porch +
			panel_info.mode.hback_porch;
	vsync_period = panel_info.mode.vactive +
			panel_info.mode.vsync_len +
			panel_info.mode.vfront_porch +
			panel_info.mode.vback_porch;
	hstart = panel_info.mode.hsync_len +
		panel_info.mode.hfront_porch - 12;
	vstart = panel_info.mode.vsync_len +
		panel_info.mode.vfront_porch;
	hend = hstart + panel_info.mode.hactive - 1;
	vend = vstart + panel_info.mode.vactive - 1;

	oscratio = (panel_info.controller_clock /
		panel_info.mode.pixclock) - 1;
	if (oscratio < 2)
		oscratio = 2;
	writel(oscratio | HALF_DUTY, panel_info.base + LCD_S0_OSC_RATIO);

	tim_ctrl = PCLK_IO | HSYNC_IO | VSYNC_IO;
	if (panel_info.timing & PANEL_PCLK_POLAR)
		tim_ctrl |= PCLK_POLAR;
	if (panel_info.timing & PANEL_PCLK_EDGE)
		tim_ctrl |= PCLK_EDGE;
	if (panel_info.timing & PANEL_HSYNC_POLAR)
		tim_ctrl |= HSYNC_POLAR;
	if (panel_info.timing & PANEL_VSYNC_POLAR)
		tim_ctrl |= VSYNC_POLAR;
	writel(tim_ctrl, panel_info.base + LCD_S0_TIM_CTRL);

	writel(RGB_SEQ_RGB, panel_info.base + LCD_S0_RGB_SEQ);

	writel(BLANK_VALID | 0xff0000, panel_info.base + LCD_S0_BLANK);
	writel(0x0, panel_info.base + LCD_S0_BACK_COLOR);

	writel(hsync_period - 1, panel_info.base + LCD_S0_HSYNC_PERIOD);
	writel(panel_info.mode.hsync_len - 1,
	       panel_info.base + LCD_S0_HSYNC_WIDTH);
	writel(vsync_period - 1, panel_info.base + LCD_S0_VSYNC_PERIOD);
	writel((panel_info.mode.vsync_len - 1) | WIDTH_UNIT_LINES,
	       panel_info.base + LCD_S0_VSYNC_WIDTH);

	writel(hstart, panel_info.base + LCD_S0_ACT_HSTART);
	writel(vstart, panel_info.base + LCD_S0_ACT_VSTART);
	writel(hend, panel_info.base + LCD_S0_ACT_HEND);
	writel(vend, panel_info.base + LCD_S0_ACT_VEND);

	writel(OUT_FORMAT_RGB888 | TOP_LAYER(0) | FRAME_VALID,
	       panel_info.base + LCD_S0_DISP_MODE);

	writel(LCD_SCRN_EN, panel_info.base + LCD_SCR_CTRL);
	writel(INT_LINE_VALID, panel_info.base + LCD_S0_INT_LINE);

	writel(0x00428119, panel_info.base + LCD_S0_RGB_YUV_COEF1);
	writel(0x00264A70, panel_info.base + LCD_S0_RGB_YUV_COEF2);
	writel(0x00705E12, panel_info.base + LCD_S0_RGB_YUV_COEF3);
	writel(0x00108080, panel_info.base + LCD_S0_RGB_YUV_OFFSET);

	writel(YUV_SEQ_YVYU | EVEN_UV, panel_info.base + LCD_S0_YUV_CTRL);
	writel(0x339 | (0x106 << 12), panel_info.base + LCD_S0_TV_FIELD);
}

static void lcd_config_layer(int layer)
{
	unsigned int hstart, vstart;
	unsigned int hend, vend;
	unsigned int wstride_byte;
	unsigned int dma_xsize, dma_skip;
	unsigned int dma_ctrl, dma_suppress;
	unsigned int dma_unit = 128;
	unsigned int lctrl = 0;

	hstart = panel_info.mode.hsync_len + panel_info.mode.hfront_porch - 12;
	vstart = panel_info.mode.vsync_len + panel_info.mode.vfront_porch;
	hend = hstart + panel_info.mode.hactive - 1;
	vend = vstart + panel_info.mode.vactive - 1;

	/* set layer timings */
	writel(hstart, panel_info.base + LCD_HSTART(layer));
	writel(vstart, panel_info.base + LCD_VSTART(layer));
	writel(hend, panel_info.base + LCD_HEND(layer));
	writel(vend, panel_info.base + LCD_VEND(layer));

	writel(0xFF, panel_info.base + LCD_ALPHA(layer));

	/* DMA initialization */
	wstride_byte = FB_BYTES(panel_info.bpp) * panel_info.mode.hactive;
	/* byte width of frame buffer has to be a multiple of 8 */
	if (wstride_byte % 8)
		wstride_byte = (wstride_byte / 8 + 1) * 8;
	dma_xsize = ((wstride_byte + dma_unit - 1) / dma_unit) - 1;
	dma_suppress = ((dma_xsize + 1) * dma_unit - wstride_byte) >> 3;
	dma_skip = wstride_byte - (dma_xsize * dma_unit);
	dma_ctrl = (dma_suppress << 8) |
		(((dma_unit >> 3) - 1) << 4) |
		DMA_CONTINUOUS_MODE;

	writel(dma_xsize, panel_info.base + LCD_XSIZE(layer));
	writel(panel_info.mode.vactive - 1, panel_info.base + LCD_YSIZE(layer));
	writel(dma_skip, panel_info.base + LCD_SKIP(layer));
	writel(dma_ctrl, panel_info.base + LCD_DMA_CTRL(layer));
	writel(panel_info.smem_start, panel_info.base + LCD_BASE0(layer));

	lctrl = CTRL_FIFO_RESET | CTRL_REPLICATE | CRTL_PREMULTI_ALPHA;
	switch (panel_info.bpp) {
	case 16:
		lctrl |= BPP_RGB565;
		break;
	case 24:
		lctrl |= BPP_RGB888;
		break;
	case 32:
		lctrl |= BPP_ARGB8888;
		break;
	}
	lctrl |= CTRL_CONFIRM;
	writel(lctrl, panel_info.base + LCD_CTRL(layer));
	lctrl &= ~CTRL_FIFO_RESET;
	writel(lctrl, panel_info.base + LCD_CTRL(layer));

	writel(LAYER_ENABLE(layer), panel_info.base + LCD_S0_LAYER_SEL);
}


static void lcd_fill_color(void)
{
	int i, j, color;
	unsigned char bpp32_colormap[][4] = {
		{0x0, 0x0, 0x0, 0xFF},		/* BLACK */
		{0xFF, 0xFF, 0xFF, 0xFF},	/* WHITE */
		{0x0, 0x0, 0xFF, 0xFF},		/* RED */
		{0x0, 0xFF, 0x0, 0xFF},		/* GREEN */
		{0xFF, 0x0, 0x0, 0xFF},		/* BLUE */
		{0x0, 0xFF, 0xFF, 0xFF},	/* YELLOW */
		{0xFF, 0x0, 0xFF, 0xFF},	/* MAGENTA */
		{0xFF, 0xFF, 0x0, 0xFF},	/* CYAN */
	};
	unsigned char *cmap;

	unsigned int bytes_per_line =
		(panel_info.mode.hactive * panel_info.bpp + 31) / 32 * 4;
	unsigned int bytes_per_pixel = panel_info.bpp / 8;

	memset((void *)panel_info.smem_start, 0,
	       bytes_per_line * panel_info.mode.vactive);

	for (j = 0; j < panel_info.mode.vactive; j++)
		for (i = 0; i < panel_info.mode.hactive; i++) {
			color = (i * 8) / panel_info.mode.hactive;
			cmap = bpp32_colormap[color];

			memcpy((void *)(panel_info.smem_start +
					j * bytes_per_line +
					i * bytes_per_pixel),
				cmap, bytes_per_pixel);
		}
}

extern bool i2c_dev_reg_bit_set(int port_index, u32 dev_addr,
				u32 reg_addr, u8 bit_map);
static void lcd_pre_powerup(void)
{
	i2c_dev_reg_bit_set(0, 0x80, panel_info.vcc & 0xFFFF, 1);
}

static void lcd_powerup(void)
{
	memcpy(&panel_info, &lcd_panels[0], sizeof(panel_info));

	panel_info.base = (void *)LCD_CONTROLLER_BASE;
	panel_info.smem_start = LAYER0_MEM_BASE;
	panel_info.controller_clock = LCD_CLK_RATE;

	lcd_fill_color();

	lcd_config_screen();
	lcd_config_layer(LCD_BOOTSPLASH_LAYER);
}

static void gpio_set_high(int gpio)
{
	int group, num;
	unsigned long offset, status, muxval;

	group = gpio / 32;
	num = gpio % 32;
	offset = GPIO_CTRL(group, num);

	/* set pinmux to GPIO */
	muxval = readl(GPIO_PAD_EN(group));
	muxval |= (1 << num);
	writel(muxval, GPIO_PAD_EN(group));

	/* set direction to output */
	status = readl(offset);
	status |= GPIO_CTL_OUT_EN_MASK;
	writel(status, offset);

	/* set value to high */
	status = readl(offset);
	status |= GPIO_CTL_DATAOUT_MASK;
	writel(status, offset);
}

static void gpio_reset_padmux(unsigned int group,
			unsigned int padmux_mask)
{
	unsigned int val;
	unsigned long offset;

	offset = GPIO_PAD_EN(group);

	val = readl(offset);
	val &= ~padmux_mask;
	writel(val, offset);
}

static void lcd_post_powerup(void)
{
	gpio_reset_padmux(0, 0xff0000);
	gpio_reset_padmux(1, 0xc0000000);
	gpio_reset_padmux(2, 0x807fbfc2);

	i2c_dev_reg_bit_set(0, 0x80, panel_info.vee & 0xFFFF, 1);
	/* power up backlight */
	gpio_set_high(BACKLIGHT_PWM_GPIO_NO);
}

void video_hw_init(void)
{
	lcd_pre_powerup();
	lcd_powerup();
	lcd_post_powerup();
	lcd_layer_reg_dump(LCD_BOOTSPLASH_LAYER);
}
