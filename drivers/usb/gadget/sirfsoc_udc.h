
/*
 * USB Device Controller driver for CSR SiRFatlasVI
 *
 * Copyright (c) 2012 Cambridge Silicon Radio Limited, a CSR plc group company.
 *
 * Licensed under GPLv2 or later.
 */

#ifndef __SIRF_UDC_H_
#define __SIRF_UDC_H_

typedef void (*rx_callback) (char *buffer, int len);

/* sirfsoc usb controller device exported interfaces  */
int udc_init(rx_callback pfrxdone);
int udc_endpoint_write(struct usb_endpoint_instance *endpoint,
					   char *buffer, int len);
void udc_setup_ep(struct usb_device_instance *device, unsigned int ep_num,
				  struct usb_endpoint_instance *endpoint);
void udc_connect(void);
void udc_disconnect(void);
void udc_enable(struct usb_device_instance *device);
void udc_disable(void);
void udc_startup_events(struct usb_device_instance *device);
void udc_poll_irq(void);
void udc_do_ctrl_xfer(char *buffer, int len, u8 dir);
int udc_endpoint_queue_read(struct usb_endpoint_instance *endpoint,
							int len);
void usbcd_device_init_complete(void);

/* Flow control */
void udc_set_nak(int epid);
void udc_unset_nak(int epid);

#endif
