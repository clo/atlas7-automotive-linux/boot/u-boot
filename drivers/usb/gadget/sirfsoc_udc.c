/*
 * USB Device Controller driver for CSR SiRFatlasVI
 *
 * Copyright (c) 2013-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <config.h>
#include <asm/io.h>
#include <asm/arch/clk.h>
#include <asm/arch/clkc.h>
#include <asm/arch/rsc.h>
#include <asm/arch/rst.h>
#include <usbdevice.h>
#include <watchdog.h>
#include "ep0.h"

#include "sirfsoc_udc.h"
#include "sirfsoc_usb_regs.h"

static struct udc_global_var gusbcd_db;
static int usbcd_init_flag;

/* prototype */
static void usbcd_init_ep_dTD(struct udc_global_var *usbcd_db,
			struct dTD *dTDesc, u8 *buffer, int xfersize, u8 dir);
static void usbcd_ep_schedule_xfer(struct udc_global_var *usbcd_db,
		struct usb_endpoint_instance *ep, struct dTD *dTDesc, u8 dir);
static void usbcd_process_ep0(struct udc_global_var *usbcd_db,
					struct usb_device_request *req);
static int usbcd_check_usb_reset(struct udc_global_var *usbcd_db);
static int usbcd_poll_specfic(struct udc_global_var *usbcd_db);
static struct usb_endpoint_instance *find_ep(struct udc_global_var *usbcd_db,
									u16 ep);

#define PRINT_REG(addr, offset) \
	debug(#offset "= 0x%x\n", READ_REG((addr), (offset)))

void usbcd_device_init_complete(void)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;

	/* Send EP-OUT request to the controller for receive */
	usbcd_db->usb_wait_for_setup = 0;
}

static void usbcd_dump_hwregs(u32 addr)
{
	/* Identification Registers */
	PRINT_REG(addr, ID);
	PRINT_REG(addr, HWGENERAL);
	PRINT_REG(addr, HWHOST);
	PRINT_REG(addr, HWDEVICE);
	PRINT_REG(addr, HWTXBUF);
	PRINT_REG(addr, HWRXBUF);
	PRINT_REG(addr, SBUSCFG);

	/* Device/Host Capability Registers */
	PRINT_REG(addr, CAPLENGTH);
	PRINT_REG(addr, HCIVERSION);
	PRINT_REG(addr, HCSPARAMS);
	PRINT_REG(addr, HCCPARAMS);
	PRINT_REG(addr, DCIVERSION);
	PRINT_REG(addr, DCCPARAMS);

	/* Device/Host Timer Registers (Non-EHCI) */
	PRINT_REG(addr, GPTIMER0LD);
	PRINT_REG(addr, GPTIMER0CTRL);
	PRINT_REG(addr, GPTIMER1LD);
	PRINT_REG(addr, GPTIMER1CTRL);

	/* Device/Host Operational Registers */
	PRINT_REG(addr, USBCMD);
	PRINT_REG(addr, USBSTS);
	PRINT_REG(addr, USBINTR);
	PRINT_REG(addr, FRINDX);
	PRINT_REG(addr, DEVICEADDR);
	PRINT_REG(addr, PERIODICLISTBASE);
	PRINT_REG(addr, ENDPOINTLISTADDR);
	PRINT_REG(addr, ASYNCLISTADDR);
	PRINT_REG(addr, TTCTRL);
	PRINT_REG(addr, BURSTSIZE);
	PRINT_REG(addr, TXFILLTUNNING);
	PRINT_REG(addr, ULPIVIEWPORT);
	PRINT_REG(addr, ENDPTNAK);
	PRINT_REG(addr, ENDPTNAKEN);
	PRINT_REG(addr, PORTSC1);
	PRINT_REG(addr, OTGSC);
	PRINT_REG(addr, USBMODE);
	PRINT_REG(addr, ENDPTSETUPSTAT);
	PRINT_REG(addr, ENDPTPRIME);
	PRINT_REG(addr, ENDPTFLUSH);
	PRINT_REG(addr, ENDPTSTATUS);
	PRINT_REG(addr, ENDPTCOMPLETE);
	PRINT_REG(addr, ENDPTCTRL0);
	PRINT_REG(addr, ENDPTCTRLN);
	PRINT_REG(addr, USBOTG_PHY);
}

#define PRINT_FIELD(a, field) debug(#a "->" #field "= 0x%x\n", (a)->field)
static void usbcd_dump_td(struct dTD *td)
{
	PRINT_FIELD(td, terminate);
	PRINT_FIELD(td, next_link);
	PRINT_FIELD(td, status);
	PRINT_FIELD(td, multo);
	PRINT_FIELD(td, ioc);
	PRINT_FIELD(td, total_bytes);
	PRINT_FIELD(td, buf_ptr[0]);
	PRINT_FIELD(td, buf_ptr[1]);
	PRINT_FIELD(td, buf_ptr[2]);
	PRINT_FIELD(td, buf_ptr[3]);
}

static void usbcd_hw_init(u32 addr)
{
	u32 reg_val = 0x0, loop_cnt = MAX_LOOP_CNT;

	/* enable usb phy */
	WRITE_REG(addr,
		  (READ_REG(addr, USBOTG_PHY) & (0xf7ffffff)),
		  USBOTG_PHY);

	/* Controller Operational Mode selection set to DEVICE */
	reg_val = READ_REG(addr, USBMODE);
	RESERVE_RESET(USBMODE, reg_val);
	SET_CMP(USBMODE, reg_val, CM, 0x02);
	WRITE_REG(addr, reg_val, USBMODE);

	/* Prevent the attach event before the controller initialized */
	reg_val = READ_REG(addr, USBCMD);
	RESERVE_RESET(USBCMD, reg_val);
	SET_CMP(USBCMD, reg_val, RS, 0x00);
	SET_CMP(USBCMD, reg_val, ITC, 0x00);
	SET_CMP(USBCMD, reg_val, RST, 0x01);
	WRITE_REG(addr, reg_val, USBCMD);

	/* Reset the Controller */
	do {
		reg_val = READ_REG(addr, USBCMD);
	} while ((GET_CMP(USBCMD, reg_val, RST)) && loop_cnt--);

	if (loop_cnt <= 0) {
		error("Reset of the USB controller incomplete\n");
		return;
	}

	/* Controller Operational Mode selection set to DEVICE */
	reg_val = READ_REG(addr, USBMODE);
	RESERVE_RESET(USBMODE, reg_val);
	SET_CMP(USBMODE, reg_val, CM, 0x02);
	WRITE_REG(addr, reg_val, USBMODE);

#ifdef CONFIG_FULL_SPEED_DEVICE
	reg_val = READ_REG(addr, PORTSC1);
	RESERVE_RESET(PORTSC1, reg_val);
	SET_CMP(PORTSC1, reg_val, PFSC, 0x01);
	WRITE_REG(addr, reg_val, PORTSC1);
#endif

	reg_val = READ_REG(addr, OTGSC);
	RESERVE_RESET(OTGSC, reg_val);
	SET_CMP(OTGSC, reg_val, OT, 0x0);
	WRITE_REG(addr, reg_val, OTGSC);

	/* ypluo: enable the required interrupts */
	reg_val = READ_REG(addr, USBINTR);
	reg_val |= 0x30101d7;
	RESERVE_RESET(USBINTR, reg_val);
	WRITE_REG(addr, reg_val, USBINTR);

	/* set Run/stop bit to start to receive the events */
	reg_val = READ_REG(addr, USBCMD);
	SET_CMP(USBCMD, reg_val, RS, 0x01);
	RESERVE_RESET(USBCMD, reg_val);
	WRITE_REG(addr, reg_val, USBCMD);
}

static void usbcd_clk_enable(int enable, u32 clkbit)
{
#ifndef CONFIG_ARCH_ATLAS7
if (enable)
		setbits_le32(CLKC_CLK_EN0, CLK_USB1_EN);
	else
		clrbits_le32(CLKC_CLK_EN0, CLK_USB1_EN);
#else
	if (enable)
		out_le32((1 << 9) | (1 << 11), CLKC_MISC1_LEAF_CLK_EN_SET);
	else
		out_le32((1 << 9) | (1 << 11), CLKC_MISC1_LEAF_CLK_EN_CLR);
#endif
}

static void sirfsoc_reset_block(unsigned num)
{
#ifndef CONFIG_ARCH_ATLAS7
	setbits_le32(RESET_SR0, RESET_SR_USB1_RST);
	udelay(500);
	clrbits_le32(RESET_SR0, RESET_SR_USB1_RST);
#else
	out_le32(1 << 9, CLKC_RSTC_MISC1_SW_RST_CLR);
	udelay(500);
	out_le32((1 << 9) | (1 << 11), CLKC_MISC1_LEAF_CLK_EN_CLR);
	udelay(500);
	out_le32(1 << 9, CLKC_RSTC_MISC1_SW_RST_SET);
	udelay(500);
	out_le32((1 << 9) | (1 << 11), CLKC_MISC1_LEAF_CLK_EN_SET);
	udelay(500);
#endif
}

int udc_init(rx_callback pfrxdone)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;
	int i;

	usbcd_init_flag = 1;
	memset(usbcd_db, 0, sizeof(struct udc_global_var));

#ifndef CONFIG_ARCH_ATLAS7
	/* reset usb1 block */
	sirfsoc_reset_block(1);
	usbcd_db->addr = USB1_REG_BASE;
	usbcd_db->clkbit = CLK_USB1_EN;
#else
	/* reset usb0 block */
	sirfsoc_reset_block(1);
	usbcd_db->addr = USB0_REG_BASE;
	usbcd_db->clkbit = CLK_USB0_EN;
#endif

	/* switch to USB mode */
#ifndef CONFIG_ARCH_ATLAS7
	clrbits_le32(RSC_USB_UART_SHARE, USBUART_SHARE_EN);
	WRITE_REG(usbcd_db->addr,
		  READ_REG(usbcd_db->addr, USBOTG_PHY) | 0x8000000,
		  USBOTG_PHY);
	udelay(10);			/* here wait at least 10us */
	setbits_le32(RSC_USB_UART_SHARE, USB1_MODE_SEL);/* select usb1 mode */
#else
	WRITE_REG(usbcd_db->addr,
		  READ_REG(usbcd_db->addr, USBOTG_PHY) | 0x8000000,
		  USBOTG_PHY);
#endif
	udelay(10);			/* here wait at least 10us */
	WRITE_REG(usbcd_db->addr,
		  READ_REG(usbcd_db->addr, USBOTG_PHY) & 0xf7ffffff,
		  USBOTG_PHY);

	usbcd_db->dQH_ptr = (struct dQH *)CTRL_PHYS_ADDR_QH;
	usbcd_db->dTD_ptr = (struct dTD *)CTRL_PHYS_ADDR_TD;
	usbcd_db->dtd_in_buffer = (u8 *)CTRL_PHYS_ADDR_DATA_IN;
	usbcd_db->dtd_data_buffer = (u8 *)CTRL_PHYS_ADDR_DATA_OUT;
	memset(usbcd_db->dQH_ptr, 0, (8 * sizeof(struct dQH)));

	for (i = 0; i < MAX_TD; i++) {
		usbcd_db->td[i] = DTD_ITEM(usbcd_db->dTD_ptr, i);
		memset(usbcd_db->td[i], 0, sizeof(struct dTD));
	}

#ifndef CONFIG_ARCH_ATLAS7
	/* enable usb phy pll */
	clrbits_le32(RSC_USBPHY_PLL_CTRL, USBPHY_PLL_POWERDOWN);
	clrbits_le32(RSC_USBPHY_PLL_CTRL, USBPHY_PLL_LOCK);
	udelay(500);
#endif
	/* enable the usb sys clock */
	usbcd_clk_enable(1, usbcd_db->clkbit);

	usbcd_db->pfrxdone = pfrxdone;
	usbcd_hw_init(usbcd_db->addr);

	return 0;
}

static void usbcd_init_ep_QH(struct udc_global_var *usbcd_db,
					unsigned char ep_num, unsigned char dir,
					unsigned int max_pkt_len)
{
	struct dQH *ep_qh;

	if (USB_DIR_OUT == dir)
		ep_qh = usbcd_db->dQH_ptr + (2 * ep_num);
	else
		ep_qh = usbcd_db->dQH_ptr + (2 * ep_num) + 1;

	ep_qh->mult = 0x00;
	ep_qh->terminate = 0x01;
	ep_qh->zlt = 0x01;
	ep_qh->maxPktLen = max_pkt_len;
}

static void usbcd_ep_enable(struct udc_global_var *usbcd_db,
					unsigned char ep_num, unsigned char dir,
					unsigned char ep_type)
{
	u32 addr = usbcd_db->addr;
	u32 reg_val = 0x00;

	/* If Control Endpoint */
	if (0 == ep_num)
		return;

	WRITE_EP_REG(addr, 0x0, ep_num);
	if (dir == USB_DIR_IN) {
		SET_CMP(ENDPTCTRLN, reg_val, TXT, ep_type);
		SET_CMP(ENDPTCTRLN, reg_val, TXR, 0x1);
	} else if (dir == USB_DIR_OUT) {
		SET_CMP(ENDPTCTRLN, reg_val, RXT, ep_type);
		SET_CMP(ENDPTCTRLN, reg_val, RXR, 0x1);
	}
	WRITE_EP_REG(addr, reg_val, ep_num);

	reg_val = READ_EP_REG(addr, ep_num);
	RESERVE_RESET(ENDPTCTRLN, reg_val);

	if (dir == USB_DIR_IN)
		SET_CMP(ENDPTCTRLN, reg_val, TXE, 0x1);

	if (dir == USB_DIR_OUT)
		SET_CMP(ENDPTCTRLN, reg_val, RXE, 0x1);

	WRITE_EP_REG(addr, reg_val, ep_num);
}

static void usbcd_ep_prime(struct udc_global_var *usbcd_db, u16 ep_num,
						   u8 dir)
{
	u32 addr = usbcd_db->addr, reg_val = 0x00, mask = 0x00;
	u32 counter = MAX_LOOP_CNT;

	reg_val = READ_REG(addr, ENDPTPRIME);
	if (dir == USB_DIR_IN) {
		SET_CMP(ENDPTPRIME, reg_val, PETB, (1 << ep_num));
		mask = ((1 << ep_num) << 16);
	} else if (dir == USB_DIR_OUT) {
		SET_CMP(ENDPTPRIME, reg_val, PERB, (1 << ep_num));
		mask = (1 << ep_num);
	}

	WRITE_REG(addr, reg_val, ENDPTPRIME);
	do {
		reg_val = READ_REG(addr, ENDPTSTATUS);
	} while (!(reg_val & mask) && (--counter));
	if (!counter) {
		error("usbcd_ep_prime failed, mask 0x%x\n", mask);
		usbcd_dump_hwregs(usbcd_db->addr);
		return;
	}
	WRITE_REG(addr, READ_REG(addr, ENDPTNAK), ENDPTNAK);
}


void udc_setup_ep(struct usb_device_instance *device,
				  unsigned int ep_num,
				  struct usb_endpoint_instance *endpoint)
{
	unsigned int max = 0x0;
	struct udc_global_var *usbcd_db = &gusbcd_db;

	debug("udc_setup_ep: ep_num %d, ep_addr=0x%x\n",
	      ep_num, endpoint->endpoint_address);
	switch (endpoint->tx_attributes & USB_ENDPOINT_XFERTYPE_MASK) {
	case USB_ENDPOINT_XFER_BULK:
	case USB_ENDPOINT_XFER_INT:
		max = USB_MAX_NON_CTRL_PAYLOAD;
		break;
	case USB_ENDPOINT_XFER_CONTROL:
		max = USB_MAX_CTRL_PAYLOAD;
		break;
	default:
		goto fail;
	}

	if (ep_num != 0) {
		usbcd_init_ep_QH(usbcd_db, (unsigned char) ep_num,
				 (unsigned char)((endpoint->endpoint_address &
						  USB_DIR_IN) ?
				 USB_DIR_IN : USB_DIR_OUT), max);
		usbcd_ep_enable(usbcd_db, (unsigned char) ep_num,
				(unsigned char)((endpoint->endpoint_address &
						 USB_DIR_IN) ?
				USB_DIR_IN : USB_DIR_OUT),
				(unsigned char) (endpoint->tx_attributes &
				USB_ENDPOINT_XFERTYPE_MASK));
	}
	return;
fail:
	error("Wrong parameters!\n");
}

/* setup qh structure and ep register for ep0. */
static void usbcd_ep0_setup(struct udc_global_var *usbcd_db)
{
	struct dQH *ep_qh = NULL;

	usbcd_init_ep_QH(usbcd_db, 0, USB_DIR_OUT, USB_MAX_CTRL_PAYLOAD);
	ep_qh = usbcd_db->dQH_ptr;
	ep_qh->ios = 1;
}

static void usbcd_ep_stall(u32 addr, u16 ep_num, u8 dir, u8 bstate)
{
	u32 reg_val = 0x0;

	if (ep_num) {
		reg_val = READ_EP_REG(addr, ep_num);
		RESERVE_RESET(ENDPTCTRLN, reg_val);
	}

	if (dir == USB_DIR_IN)
		SET_CMP(ENDPTCTRLN, reg_val, TXS, bstate);

	if (dir == USB_DIR_OUT)
		SET_CMP(ENDPTCTRLN, reg_val, RXS, bstate);

	if (0 == ep_num)
		WRITE_REG(addr, reg_val, ENDPTCTRL0);
	else
		WRITE_EP_REG(addr, reg_val, ep_num);

	debug("ST %d D 0x%x\n", ep_num, dir);
	return;
}

/* Prime a status phase for ep0 */
static void usbcd_ep0_prime_status(struct udc_global_var *usbcd_db, u8 dir)
{
	struct dTD *dTDesc = usbcd_db->td[0];

	usbcd_init_ep_dTD(usbcd_db, dTDesc, NULL, 0, dir);
	usbcd_ep_schedule_xfer(usbcd_db, find_ep(usbcd_db, 0), dTDesc, dir);
}

static void usbcd_configure_device(struct udc_global_var *usbcd_db,
					struct usb_device_instance *device)
{
	u32 addr = usbcd_db->addr, reg_val = 0x00;

/* Configure EPPOINTLISTADDR register with endpoint Queue head address */
	SET_CMP(ENDPOINTLISTADDR, reg_val, EPBASE,
		(u32) USB_VIRT_TO_PHYS(usbcd_db->dQH_ptr));
	WRITE_REG(addr, reg_val, ENDPOINTLISTADDR);

	reg_val = READ_REG(addr, USBSTS);
	RESERVE_RESET(USBSTS, reg_val);
	WRITE_REG(addr, reg_val, USBSTS);

	reg_val = READ_REG(addr, ENDPTSETUPSTAT);
	RESERVE_RESET(ENDPTSETUPSTAT, reg_val);
	WRITE_REG(addr, reg_val, ENDPTSETUPSTAT);

	reg_val = READ_REG(addr, ENDPTCOMPLETE);
	WRITE_REG(addr, reg_val, ENDPTCOMPLETE);

	usbcd_init_ep_QH(usbcd_db, 0, USB_DIR_IN, USB_MAX_CTRL_PAYLOAD);
	usbcd_init_ep_QH(usbcd_db, 0, USB_DIR_OUT, USB_MAX_CTRL_PAYLOAD);
}

void udc_enable(struct usb_device_instance *device)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;
	struct urb *urb = usbcd_db->ep0_urb;

	/* Save the device structure pointer */
	usbcd_db->udc_device = device;

	/* Setup ep0 urb */
	if (urb) {
		error("Ctrl EP urd De-allocating\n");
		usbd_dealloc_urb(urb);
	}
	urb = usbd_alloc_urb(usbcd_db->udc_device,
				usbcd_db->udc_device->bus->endpoint_array);
	if (NULL == urb)
		error("urb alloc failed\n");
	usbcd_db->ep0_urb = urb;
	usbcd_configure_device(usbcd_db, device);
}

/* switch off the UDC */
void udc_disable(void)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;

	/* Free ep0 URB */
	if (usbcd_db->ep0_urb) {
		usbd_dealloc_urb(usbcd_db->ep0_urb);
		usbcd_db->ep0_urb = NULL;
	}
}

/* turn on the USB connection by enabling the pullup resistor */
void udc_connect(void)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;
	u32 addr = usbcd_db->addr;
	u32 reg_val = 0x00;

	debug("%s\n", __func__);
	if (!usbcd_init_flag)
		return;
	reg_val = READ_REG(addr, OTGSC);
	RESERVE_RESET(OTGSC, reg_val);
	SET_CMP(OTGSC, reg_val, OT, 0x01);
	SET_CMP(OTGSC, reg_val, IDPU, 0x01);
	SET_CMP(OTGSC, reg_val, IDIE, 0x01);
	SET_CMP(OTGSC, reg_val, BSVIE, 0x01);

	SET_CMP(OTGSC, reg_val, BSEIE, 0x01);
	SET_CMP(OTGSC, reg_val, AVVIE, 0x01);
	SET_CMP(OTGSC, reg_val, ASVIE, 0x01);

	WRITE_REG(addr, reg_val, OTGSC);
}

/* Turn off the USB connection by disabling the pullup resistor */
void udc_disconnect(void)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;
	u32 addr = usbcd_db->addr;
	u32 reg_val = 0x0;

	debug("%s()\n", __func__);
	if (!usbcd_init_flag)
		return;
	/* set Run/stop bit to start to receive the events */
	reg_val = READ_REG(addr, USBCMD);
	SET_CMP(USBCMD, reg_val, RS, 0x00);
	RESERVE_RESET(USBCMD, reg_val);
	WRITE_REG(addr, reg_val, USBCMD);

	/* Reset the controller */
	reg_val = READ_REG(addr, USBCMD);
	RESERVE_RESET(USBCMD, reg_val);
	SET_CMP(USBCMD, reg_val, RST, 0x01);
	WRITE_REG(addr, reg_val, USBCMD);

	/* disable the usb clk */
	usbcd_clk_enable(0, usbcd_db->clkbit);
}

static void usbcd_init_ep_dTD(struct udc_global_var *usbcd_db,
				struct dTD *dTDesc, u8 *buffer,
				int xfersize, u8 dir)
{
	u8 *buf_ptr = NULL;
	u8 i;

	if ((USB_DIR_IN == dir) && (buffer != NULL)) {
		memcpy(usbcd_db->dtd_data_buffer, buffer, xfersize);
		buf_ptr = usbcd_db->dtd_data_buffer;
	} else if (USB_DIR_OUT == dir) {
		buf_ptr = usbcd_db->dtd_in_buffer;
	}

	memset(dTDesc, 0, sizeof(struct dTD));

	dTDesc->terminate = 0x1;
	dTDesc->total_bytes = (min(xfersize, MAX_TD_BUF_SIZE));
	dTDesc->ioc = 0x01;
	dTDesc->status = PCD_TD_ACTIVE;

	for (i = 0; i < TD_MAX_BUF_PTR; i++) {
		if (xfersize > 0) {
			dTDesc->buf_ptr[i] = (u32) USB_VIRT_TO_PHYS(buf_ptr);
			xfersize -= (min(xfersize, MAX_TD_PER_BUF_SIZE));
			buf_ptr += MAX_TD_PER_BUF_SIZE;
		} else {
			break;
		}
	}
}

static void usbcd_ep_schedule_xfer(struct udc_global_var *usbcd_db,
					struct usb_endpoint_instance *ep,
					struct dTD *dTDesc, u8 dir)
{
	struct dQH *ep_qh;

	if (ep == NULL) {
		error("it is not suitable endpoint instance\n");
		return;
	}
	u16 max_pkt_len = USB_MAX_NON_CTRL_PAYLOAD;
	u16 ep_num = (ep->endpoint_address & USB_ENDPOINT_NUMBER_MASK);

	if (!ep_num)
		max_pkt_len = USB_MAX_CTRL_PAYLOAD;
	else
		debug("ep_num:%d, dir 0x%x\n", ep_num, dir);

	if (USB_DIR_OUT == dir)
		ep_qh = usbcd_db->dQH_ptr + (2 * ep_num);
	else
		ep_qh = usbcd_db->dQH_ptr + (2 * ep_num) + 1;

	usbcd_init_ep_QH(usbcd_db, ep_num, dir, max_pkt_len);
	ep_qh->nexttd_ptr = ((u32) USB_VIRT_TO_PHYS(dTDesc) >> 5);
	ep_qh->terminate = 0x0;

	usbcd_ep_prime(usbcd_db, ep_num, dir);
}

void udc_clear_regs(void *context)
{
	struct udc_global_var *usbcd_db = (struct udc_global_var *)context;
	u32 addr = usbcd_db->addr, reg_val = 0x00;

	reg_val = READ_REG(addr, USBSTS);
	RESERVE_RESET(USBSTS, reg_val);
	WRITE_REG(addr, reg_val, USBSTS);

	reg_val = READ_REG(addr, ENDPTSETUPSTAT);
	RESERVE_RESET(ENDPTSETUPSTAT, reg_val);
	WRITE_REG(addr, reg_val, ENDPTSETUPSTAT);

	reg_val = READ_REG(addr, ENDPTCOMPLETE);
	WRITE_REG(addr, reg_val, ENDPTCOMPLETE);
}

static struct usb_endpoint_instance *find_ep(struct udc_global_var *usbcd_db,
									u16 ep)
{
	int i;
	struct usb_device_instance *udc_device = usbcd_db->udc_device;

	for (i = 0; i < udc_device->bus->max_endpoints; i++) {
		if (udc_device->bus->endpoint_array[i].endpoint_address == ep)
			return &(udc_device->bus->endpoint_array[i]);
	}
	error("EP inst ERROR\n");
	return NULL;
}

/* Called to queue OUT (read) transfer for desired length*/
int udc_endpoint_queue_read(struct usb_endpoint_instance *endpoint,
							int len)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;
	struct dTD *dTDesc = usbcd_db->td[BULK_OUT_EP_NUM];

	debug("udc_endpoint_queue_read: len %d\n", len);

	usbcd_db->queued_rx_bytes = min(len, MAX_TD_BUF_SIZE);
	/* Again put the request on the controller for receive */
	usbcd_init_ep_dTD(usbcd_db, dTDesc, NULL, usbcd_db->queued_rx_bytes,
			  USB_DIR_OUT);

	usbcd_ep_schedule_xfer(usbcd_db, endpoint,
			       usbcd_db->td[BULK_OUT_EP_NUM], USB_DIR_OUT);
	return usbcd_db->queued_rx_bytes;
}

/* Called to start packet transmission. */

/* FIXME: this func need rewritten */
int udc_endpoint_write(struct usb_endpoint_instance *endpoint,
					   char *buffer, int len)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;
	struct dTD *dTDesc = usbcd_db->td[BULK_IN_EP_NUM];
	u32 bytes_to_xfer = len;
	u32 xfer_size, offset = 0;
	u32 flag;

	do {
		xfer_size = min(bytes_to_xfer, MAX_TD_BUF_SIZE);

		usbcd_init_ep_dTD(usbcd_db, dTDesc, ((u8 *)buffer) + offset,
				  xfer_size, USB_DIR_IN);
		usbcd_ep_schedule_xfer(usbcd_db, endpoint, dTDesc, USB_DIR_IN);
		do {
			flag = usbcd_poll_specfic(usbcd_db);

			/* usb reset happen */
			if (flag & (EP_SETUP_INTR | EP_SUSPEND_INTR))
				return 0;

			/* Handle setup intr */
			if (flag & EP_SETUP_INTR)
				return 0;
		} while (!(flag & EP_IN_INTR));
		xfer_size = xfer_size - dTDesc->total_bytes;
		offset += xfer_size;
		bytes_to_xfer -= xfer_size;

	} while (bytes_to_xfer);

	return 0;
}

/* udc_startup - allow udc code to do any additional startup */
void udc_startup_events(struct usb_device_instance *device)
{
	usbd_device_event_irq(device, DEVICE_INIT, 0);
	usbd_device_event_irq(device, DEVICE_CREATE, 0);
	udc_enable(device);
}

static int usbcd_check_usb_reset(struct udc_global_var *usbcd_db)
{
	u32 addr = usbcd_db->addr;
	u32 reg_val = 0x00;
	u32 usbsts;

	usbsts = READ_REG(addr, USBSTS);

	if (GET_CMP(USBSTS, usbsts, URI)) {
		RESERVE_RESET(USBSTS, usbsts);
		WRITE_REG(addr, usbsts, USBSTS);

		reg_val = READ_REG(addr, ENDPTCOMPLETE);
		WRITE_REG(addr, reg_val, ENDPTCOMPLETE);
		usbd_device_event_irq(usbcd_db->udc_device, DEVICE_RESET, 0);
		debug("RST\n");
		return 1;
	}

	return 0;
}

static int usbcd_wait_ep_cmplt(struct udc_global_var *usbcd_db, u32 ep_num,
							   u8 dir)
{
	u32 counter = MAX_LOOP_CNT;
	u32 mask = (1 << ep_num);

	if (USB_DIR_IN == dir)
		mask = (mask << 16);

	while (counter--) {
		u32 reg_val = 0x00;

		if (usbcd_check_usb_reset(usbcd_db)) {
			usbcd_dump_hwregs(usbcd_db->addr);
			return -1;
		}
		reg_val = READ_REG(usbcd_db->addr, ENDPTCOMPLETE);
		if (reg_val & mask) {
			WRITE_REG(usbcd_db->addr, (reg_val & mask),
				  ENDPTCOMPLETE);
			break;
		}
	}
	if (!counter) {
		error("usbcd_wait_ep_cmplt failed, mask 0x%x, CMP 0x%x\n",
		      mask, READ_REG(usbcd_db->addr, ENDPTCOMPLETE));
		usbcd_dump_hwregs(usbcd_db->addr);
		return -2;
	}

	return 0;
}

/*
 * handle one control EP transfer:
 * setup phase, IN/OUT data transfer phase and status phase
 */
static void usbcd_process_ep0(struct udc_global_var *usbcd_db,
					struct usb_device_request *req)
{
	struct urb *ep0_urb = usbcd_db->ep0_urb;
	u8 ep0_state = EPC_IDLE;
	u32 reg_val = 0x00;

	debug("actual_length %d\n", ep0_urb->actual_length);

	/* process the setup packet */
	ep0_urb->actual_length = 0;

	if ((req->bmRequestType & USB_DIR_IN) || (req->wLength == 0)) {
		ep0_state = EPC_IN_DATA_PHASE;
		/* Return 0 if able to handle the request else return -1 */
		if (ep0_recv_setup(ep0_urb) == -1) {
			error("Invaid setup: RType:x%x, Req:x%x, Val:x%x\n",
			      req->bmRequestType, req->bRequest, req->wValue);
			usbcd_ep_stall(usbcd_db->addr, 0, USB_DIR_IN, 1);
			return;
		}

		if (0 == ep0_urb->actual_length) {
			if ((req->bmRequestType & USB_DIR_IN))
				ep0_state = EPC_OUT_STATUS_PHASE;
			else
				ep0_state = EPC_IN_STATUS_PHASE;
		}
	} else {
		ep0_state = EPC_OUT_DATA_PHASE;
		ep0_urb->buffer = (u8 *)ep0_urb->buffer_data;
		ep0_urb->buffer_length = sizeof(ep0_urb->buffer_data);
		ep0_urb->actual_length = req->wLength;
	}

	switch (req->bRequest) {
	case USB_REQ_SET_ADDRESS:
		reg_val = 0x00;
		usbcd_db->udc_device->address = req->wValue;
		SET_CMP(DEVICEADDR, reg_val, USBADRA, 0x01);
		SET_CMP(DEVICEADDR, reg_val, DEVICE_ADDR, req->wValue);
		WRITE_REG(usbcd_db->addr, reg_val, DEVICEADDR);

		usbd_device_event_irq(usbcd_db->udc_device,
				      DEVICE_ADDRESS_ASSIGNED,
				      usbcd_db->udc_device->address);
		ep0_state = EPC_IN_STATUS_PHASE;
		break;

	case USB_REQ_SET_INTERFACE:
		usbd_device_event_irq(usbcd_db->udc_device,
				      DEVICE_SET_INTERFACE, 0);
		break;

	case USB_REQ_SET_CONFIGURATION:
		usbd_device_event_irq(usbcd_db->udc_device,
				      DEVICE_CONFIGURED, 0);

		ep0_state = EPC_IN_STATUS_PHASE;
		break;
	}
	debug("ep0_state %d, actual_length %d\n",
	      ep0_state, ep0_urb->actual_length);

	switch (ep0_state) {
	case EPC_IN_DATA_PHASE:
		/* Send the send */
		usbcd_init_ep_dTD(usbcd_db, usbcd_db->td[0],
				  ep0_urb->buffer, ep0_urb->actual_length,
				  USB_DIR_IN);
		usbcd_ep_schedule_xfer(usbcd_db, find_ep(usbcd_db, 0x0),
				       usbcd_db->td[0], USB_DIR_IN);

		/* wait till the data is send */
		if (usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_IN) != 0)
			break;

		/* send status phase of the data */
		usbcd_ep0_prime_status(usbcd_db, USB_DIR_OUT);

		/* wait till its send */
		usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_OUT);
		break;

	case EPC_OUT_DATA_PHASE:
		/* Receive the Data */
		{
			struct dTD *dtd0;
			u32 bytes_received;

			usbcd_init_ep_dTD(usbcd_db, usbcd_db->td[0],
					  ep0_urb->buffer,
					  ep0_urb->actual_length, USB_DIR_OUT);
			usbcd_ep_schedule_xfer(usbcd_db, find_ep(usbcd_db, 0x0),
					       usbcd_db->td[0], USB_DIR_OUT);

			usbcd_dump_td(usbcd_db->td[0]);
WAIT_EP_CMP:
			/* wait till we receive the data */
			if (usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_OUT) != 0)
				break;

			dtd0 = usbcd_db->td[0];
			bytes_received =
			min(MAX_TD_BUF_SIZE, req->wLength) - dtd0->total_bytes;

			if (dtd0->status) {
				if (dtd0->status & PCD_TD_ACTIVE) {
					/* EP0 uncomplete */
					goto WAIT_EP_CMP;
				} else {
					error("dtd0 status abnormal 0x%x\n",
					      dtd0->status);
					break;
				}
			}
			if (!dtd0->total_bytes) {	/* all data received */
				memcpy(ep0_urb->buffer, usbcd_db->dtd_in_buffer,
				       bytes_received);
				if (ep0_recv_setup(ep0_urb) == -1)
					error("Invalid OUT setup\n");
			}

			/* send status phase once received the data i */
			usbcd_ep0_prime_status(usbcd_db, USB_DIR_IN);

			/* wait till the staus is send */
			usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_IN);
			break;
		}
	case EPC_IN_STATUS_PHASE:
		/* Send the zero length data for status phase */
		usbcd_ep0_prime_status(usbcd_db, USB_DIR_IN);

		/* wait till it completes */
		usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_IN);
		break;

	case EPC_OUT_STATUS_PHASE:
		/* Send the zero length data for status */
		usbcd_ep0_prime_status(usbcd_db, USB_DIR_OUT);

		/* wait till the status is complete */
		usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_OUT);
		break;

	default:
		error("Unhandled case\n");
		break;
	}

	/* if unknown request stall and return (-1) */
	/* if data phase send the data to the host (1) */
	/* if status phase send 0 lenght packet to host (2) */

	debug("usbcd_process_ep0() done\n");
}

void udc_do_ctrl_xfer(char *buffer, int len, u8 dir)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;

	if (USB_DIR_IN == dir) {
		/* Send the data */
		usbcd_init_ep_dTD(usbcd_db, usbcd_db->td[0], (u8 *)buffer, len,
				  USB_DIR_IN);
		usbcd_ep_schedule_xfer(usbcd_db, find_ep(usbcd_db, 0x0),
				       usbcd_db->td[0], USB_DIR_IN);
		/* wait till the data is send */
		if (usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_IN) != 0)
			return;
	} else if (USB_DIR_OUT == dir) {
		/* Initiate OUT for recieving data */
		usbcd_init_ep_dTD(usbcd_db, usbcd_db->td[0], NULL, len,
				  USB_DIR_OUT);
		usbcd_ep_schedule_xfer(usbcd_db, find_ep(usbcd_db, 0x0),
				       usbcd_db->td[0], USB_DIR_OUT);
		/* wait to complete the recieve */
		if (usbcd_wait_ep_cmplt(usbcd_db, 0, USB_DIR_OUT) != 0)
			return;
	}
	return;
}

static int usbcd_poll_specfic(struct udc_global_var *usbcd_db)
{
	u32 addr = usbcd_db->addr;
	u32 usbsts = 0x0, reg_val = 0x0;
	int intr_flag = 0x0;

	do {
		usbsts = READ_REG(addr, USBSTS);

		/* poll reset */
		if (GET_CMP(USBSTS, usbsts, URI)) {
			debug("usb reset interrupt detected\n");
			WRITE_REG(addr, usbsts, USBSTS);
			usbd_device_event_irq(usbcd_db->udc_device,
					      DEVICE_RESET, 0);

			/* prepare to receive setup packet */
			usbcd_ep0_setup(usbcd_db);
			intr_flag |= EP_RESET_INTR;
			return 0;
		}

		/* check usb cable unplug */
		if (GET_CMP(USBSTS, usbsts, SLI)) {
			debug("usb suspend interrupt detected\n");
			usbd_device_event_irq(usbcd_db->udc_device,
					      DEVICE_BUS_INACTIVE, 0);
			intr_flag |= EP_SUSPEND_INTR;
		}

		/* poll IN OUT and SETUP */
		reg_val = READ_REG(addr, ENDPTCOMPLETE);
		if (reg_val) {
			RESERVE_RESET(ENDPTCOMPLETE, reg_val);
			WRITE_REG(addr, reg_val, ENDPTCOMPLETE);
		}

		if (GET_CMP(ENDPTCOMPLETE, reg_val, ERCE)) {
			debug("OUT data coming: 0x%x\n", reg_val);
			intr_flag |= EP_OUT_INTR;
		}
		if (GET_CMP(ENDPTCOMPLETE, reg_val, ETCE)) {
			debug("IN data coming: 0x%x\n", reg_val);
			intr_flag |= EP_IN_INTR;
		}

		/* Poll setup */
		reg_val = READ_REG(addr, ENDPTSETUPSTAT);
		RESERVE_RESET(ENDPTSETUPSTAT, reg_val);
		if (reg_val) {
			WRITE_REG(addr, reg_val, ENDPTSETUPSTAT);
			debug("setup intr:0x%x\n", reg_val);
			do {
				reg_val = READ_REG(addr, ENDPTSETUPSTAT);
			} while (reg_val & 0x1);
			intr_flag |= EP_SETUP_INTR;
		}
	} while (0);

	return intr_flag;
}

/* Poll for the interrupt */
void udc_poll_irq(void)
{
	struct udc_global_var *usbcd_db = &gusbcd_db;
	u32 flag;

	WATCHDOG_RESET();
	flag = usbcd_poll_specfic(usbcd_db);

	/* Handle setup intr */
	if (EP_SETUP_INTR & flag) {
		debug("Handle setup interrupt\n");
		/* Copy the setup packet to local buffer */
		memcpy((u8 *)(&(usbcd_db->ep0_urb->device_request)),
		       usbcd_db->dQH_ptr->setup_buffer, 8);

		/* Process the request */
		usbcd_process_ep0(usbcd_db,
				  &(usbcd_db->ep0_urb->device_request));
	}

	if (EP_OUT_INTR & flag) {
		debug("Call the rx callback to pass the data\n");
		/* Call the rx callback of the upper layer to pass the data */
		usbcd_db->pfrxdone((char *)usbcd_db->dtd_in_buffer,
				   usbcd_db->queued_rx_bytes -
				   usbcd_db->td[BULK_OUT_EP_NUM]->total_bytes);
	}
}
