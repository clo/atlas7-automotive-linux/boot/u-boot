/*
 * u-boot block driver
 *
 * Copyright (c) 2013-2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <config.h>
#include <common.h>
#include <asm/io.h>
#include <part.h>
#include "usbmsd.h"

extern int nanddisk_start;
static block_dev_desc_t *block_dev;

#define BOOT_DEV_SEL	0xB0030000
#define BOOT_DEV_RAM	(0xA8008000 + 0x34)
#define ATLAS7_BOOTMODE_REG	0x1023001c
typedef enum _BOOT_DEVICE {
	BOOT_DEV_SD0_NORMAL,
	BOOT_DEV_SD0_BOOTPART,
	BOOT_DEV_SD1_NORMAL,
	BOOT_DEV_SD2_NORMAL,
	BOOT_DEV_NAND,
	BOOT_DEV_USB1,
	BOOT_DEV_SPI1,
	BOOT_DEV_UART,
	BOOT_DEV_EFUSE,
	BOOT_DEV_INVALID,
} BOOT_DEVICE;

static BOOT_DEVICE get_boot_dev(void)
{
#ifdef CONFIG_ARCH_PRIMAII
	unsigned boot_mode = readl(BOOT_DEV_SEL) & 0x3f;
	printf("bootmode is 0x%x\n", boot_mode);
	if (0x01 == (boot_mode >> 4))
		return BOOT_DEV_SD1_NORMAL;
	else if (0x0d == (boot_mode & 0x0f) || (0x0e == (boot_mode & 0x0f)))
		return BOOT_DEV_SD0_NORMAL;
	else
		return BOOT_DEV_NAND;

	return BOOT_DEV_INVALID;
#endif

#if defined(CONFIG_ARCH_ATLASVI) || defined(CONFIG_ARCH_ATLAS7)
#ifdef CONFIG_ARCH_ATLASVI
	unsigned boot_mode_jmp = readl(BOOT_DEV_SEL) & 0x7;
	unsigned boot_mode = readl(BOOT_DEV_RAM) & 0x7;

	if (boot_mode_jmp != 0x00)
		boot_mode = boot_mode_jmp;
#else
	unsigned boot_mode = readl(ATLAS7_BOOTMODE_REG) & 0x7;
#endif
	if (0x01 == boot_mode)
		return BOOT_DEV_USB1;
	else if (0x02 == boot_mode)
		return BOOT_DEV_SD2_NORMAL;
	else if (0x03 == boot_mode)
		return BOOT_DEV_SD0_NORMAL;
	else if (0x05 == boot_mode)
		return BOOT_DEV_SPI1;
	else if (0x07 == boot_mode)
		return BOOT_DEV_SD0_BOOTPART;
	else if (0x00 == boot_mode || (0x06 == boot_mode))
		return BOOT_DEV_NAND;
	else if (0x04 == boot_mode)
		return BOOT_DEV_EFUSE;

	return BOOT_DEV_INVALID;
#endif
}

block_dev_desc_t *block_dev_init(void)
{
	static int inited = 0;
	bool mmc_dev;
	BOOT_DEVICE boot_dev;

	if (!inited) {
		boot_dev = get_boot_dev();

		mmc_dev = (BOOT_DEV_SD0_NORMAL == boot_dev) ||
			(BOOT_DEV_SD0_BOOTPART == boot_dev);
#ifdef CONFIG_ARCH_ATLAS7
		mmc_dev |= (BOOT_DEV_SD2_NORMAL == boot_dev);
#endif

		if (mmc_dev) {
#ifdef CONFIG_MMC
			block_dev = get_dev("mmc", 0);
#else
			error("Booting from mmc, but this image does not"
					" support mmc device ...\n");
#endif
		} else if (BOOT_DEV_NAND ==  boot_dev) {
#ifdef CONFIG_SIRF_NANDDISK
			block_dev = get_dev("nand", 0);
			if (block_dev && !block_dev->blksz)
				block_dev = NULL;
#else
			error("Booting from nanddisk, but this image does not"
					" support nanddisk device ...\n");
#endif
		} else if (BOOT_DEV_SPI1 == boot_dev) {
#if defined(CONFIG_SPI)
			block_dev = get_dev("spi", BLOCK_DEV_SPI_INDEX);
#else
			error("Spi1 boot work with MMC or NAND configure\n");
#endif
		} else {
			error("Booting Device Not Supported\n");
		}
		inited = 1;
	}
	return block_dev;
}
