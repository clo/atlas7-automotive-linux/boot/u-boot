/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <asm/io.h>
#include <asm/arch/board.h>
#include <malloc.h>

#include <asm/arch/regulator_pwm.h>
#include <asm/arch/pwm.h>

/*configure regulator period 60ns */
#define REGULATOR_PERIOD		60
#define REGULATOR_VOL_LEVEL		16

enum {
	PWMF_DISABLED = 1 << 0,
	PWMF_ENABLED = 1 << 1,
};

/* PWM D.C.= -(Vout/0.6876) +1.8398 */
static int dev_pm_opp_get_dutycycle(u32 volt_uv)
{
	return (18398 - (volt_uv*100)/6876)/100;
}

static int sirf_reg_pwm_set_voltage(struct regulator *regulator,
	unsigned int voltage_uv)
{
	int dutycycle;
	int duty_ns, period_ns;
	struct pwm_regulator_data *pwm_reg = regulator->reg_data;
	struct pwm_device *pwm = pwm_reg->pwm;

	if (pwm_reg->enabled != PWMF_ENABLED) {
		period_ns = REGULATOR_PERIOD;
		dutycycle = dev_pm_opp_get_dutycycle(voltage_uv);
		duty_ns = (period_ns * dutycycle)/100;
		if (dutycycle >= 0)
			sirf_pwm_config(pwm, duty_ns, period_ns);
		else
			return -1;
	} else {
		return -1;
	}

	return 0;
}

/* enable/disable regulator */
static int sirf_reg_pwm_enable(struct regulator *regulator)
{
	struct pwm_regulator_data *pwm_reg = regulator->reg_data;
	struct pwm_device *pwm = pwm_reg->pwm;

	pwm_reg->enabled = PWMF_ENABLED;
	sirf_pwm_enable(pwm);
	return 0;
}

static int sirf_reg_pwm_disable(struct regulator *regulator)
{
	struct pwm_regulator_data *pwm_reg = regulator->reg_data;
	struct pwm_device *pwm = pwm_reg->pwm;

	pwm_reg->enabled = PWMF_DISABLED;
	sirf_pwm_disable(pwm);
	return 0;
}

struct regulator_ops reg_pwm_ops = {
	.set_voltage = sirf_reg_pwm_set_voltage,
	.enable = sirf_reg_pwm_enable,
	.disable = sirf_reg_pwm_disable,
};

int reg_pwm_probe(struct regulator *regulator)
{
	struct pwm_regulator_data *pwm_reg = regulator->reg_data;

	regulator->reg_ops = &reg_pwm_ops;
	pwm_reg->enabled = PWMF_DISABLED;

	return 0;
}
