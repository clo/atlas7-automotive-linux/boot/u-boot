/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <asm/arch/regulator.h>

int regulator_set_voltage(struct regulator *regulator, int voltage)
{
	return regulator->reg_ops->set_voltage(regulator, voltage);
}
int regulator_get_voltage(struct regulator *regulator)
{
	return regulator->volt;
}

/* enable/disable regulator */
int regulator_enable(struct regulator *regulator)
{
	return regulator->reg_ops->enable(regulator);
}

int regulator_disable(struct regulator *regulator)
{
	return regulator->reg_ops->disable(regulator);
}
