/*
 * Copyright (c) 2012-2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <malloc.h>
#include <sdhci.h>
#include <asm/arch/mmc.h>

#define CONTROLLER_NAME "SiRF SDHCI"
#define SIRF_CLK_DELAY_SETTING_OFFSET 0x4c

int sirf_sdhci_init(u32 regbase, u32 max_clk, u32 min_clk, u32 quirks)
{
	struct sdhci_host *host = NULL;
	host = (struct sdhci_host *)malloc(sizeof(struct sdhci_host));
	if (!host) {
		printf("sdhci_host malloc fail!\n");
		return 1;
	}

	host->name = CONTROLLER_NAME;
	host->ioaddr = (void *)regbase;
	host->quirks = quirks;

#ifndef CONFIG_ARCH_ATLAS7
	sdhci_writel(host, 0x60, SIRF_CLK_DELAY_SETTING_OFFSET);
#endif

	host->voltages = MMC_VDD_32_33 | MMC_VDD_33_34 | MMC_VDD_165_195;

	if (quirks & SDHCI_QUIRK_REG32_RW)
		host->version = sdhci_readl(host, SDHCI_HOST_VERSION - 2) >> 16;
	else
		host->version = sdhci_readw(host, SDHCI_HOST_VERSION);

	host->host_caps = MMC_MODE_HC | MMC_MODE_HS_52MHz;

	/*8 bit mode is supported only by slot0 and 1*/
	if ((regbase == SD_REG_BASE(0)) || (regbase == SD_REG_BASE(1)))
		host->host_caps |= MMC_MODE_8BIT;
	add_sdhci(host, max_clk, min_clk);
	return 0;
}
