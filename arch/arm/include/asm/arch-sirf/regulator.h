/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_REGULATOR_H
#define __SIRF_REGULATOR_H
struct regulator {
	 const char *name;
	 int id;
	 unsigned long volt;
	 struct regulator_ops *reg_ops;
	 void *reg_data;	 /* regulator_dev data */
};

struct regulator_ops {
	 int (*set_voltage)(struct regulator *, unsigned int);
	 int (*get_voltage)(struct regulator *);
	 /* enable/disable regulator */
	 int (*enable)(struct regulator *);
	 int (*disable)(struct regulator *);
};

int regulator_set_voltage(struct regulator *, int voltage);
int regulator_get_voltage(struct regulator *);
/* enable/disable regulator */
int regulator_enable(struct regulator *);
int regulator_disable(struct regulator *);
#endif
