/*
 * Copyright (c) 2014-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_IOCTOP_H
#define __SIRF_IOCTOP_H

#ifdef CONFIG_ARCH_ATLAS7

#define IOC_TOP_BASE				0x10E40000
#define SW_TOP_FUNC_SEL_REG_SET(idx)		(IOC_TOP_BASE + 0x80 + idx * 8)
#define SW_TOP_FUNC_SEL_REG_CLR(idx)		(IOC_TOP_BASE + 0x84 + idx * 8)

/* SW_TOP_FUNC_SEL_16_REG_SET.sd_cd_b_2
 * - sets function_select bits of SD_CD_B_2 pad
 * sets function_select bits of SD_CD_B_2 pad
 * 000 : gpio0[27]
 * 001 : sd.sd_cd_b_2 */
#define REG_16_SD_CD_B_2_SHIFT			20
#define REG_16_SD_CD_B_2_WIDTH			3
#define REG_16_SD_CD_B_2_MASK			0x00700000
#define REG_16_SD_CD_B_2_INV_MASK		0xFF8FFFFF
#define REG_16_SD_CD_B_2_HW_DEFAULT		0x0


/* SW_TOP_FUNC_SEL_16_REG_SET.sd_wp_b_2
 * - sets function_select bits of SD_WP_B_2 pad
 * sets function_select bits of SD_WP_B_2 pad
 * 000 : gpio0[29]
 * 001 : sd.sd_wp_b_2 */
#define REG_16_SD_WP_B_2_SHIFT			16
#define REG_16_SD_WP_B_2_WIDTH			3
#define REG_16_SD_WP_B_2_MASK			0x00070000
#define REG_16_SD_WP_B_2_INV_MASK		0xFFF8FFFF
#define REG_16_SD_WP_B_2_HW_DEFAULT		0x0

/* SW_TOP_FUNC_SEL_24_REG_SET.sd_cd_b_2
 * - sets function_select bits of SD_CD_B_2 pad
 * sets function_select bits of SD_CD_B_2 pad
 * 000 : jtag_gpio[2]
 * 110 : sd.sd_cd_b_2 */
#define REG_24_SD_CD_B_2_SHIFT			8
#define REG_24_SD_CD_B_2_WIDTH			3
#define REG_24_SD_CD_B_2_MASK			0x00000700
#define REG_24_SD_CD_B_2_INV_MASK		0xFFFFF8FF
#define REG_24_SD_CD_B_2_HW_DEFAULT		0x0


/* SW_TOP_FUNC_SEL_24_REG_SET.sd_wp_b_2
 * - sets function_select bits of SD_WP_B_2 pad
 * sets function_select bits of SD_WP_B_2 pad
 * 000 : jtag_gpio[4]
 * 111 : sd.sd_wp_b_2 */
#define REG_24_SD_WP_B_2_SHIFT			16
#define REG_24_SD_WP_B_2_WIDTH			3
#define REG_24_SD_WP_B_2_MASK			0x00070000
#define REG_24_SD_WP_B_2_INV_MASK		0xFFF8FFFF
#define REG_24_SD_WP_B_2_HW_DEFAULT		0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.sd_cmd_2
 * - sets function_select bits of SD_CMD_2 pad
 * sets function_select bits of SD_CMD_2 pad
 * 001 : sd.sd_cmd_2 */
#define REG_3_SD_CMD_2_SHIFT			4
#define REG_3_SD_CMD_2_WIDTH			3
#define REG_3_SD_CMD_2_MASK			0x00000070
#define REG_3_SD_CMD_2_INV_MASK			0xFFFFFF8F
#define REG_3_SD_CMD_2_HW_DEFAULT		0x0

/* SW_TOP_FUNC_SEL_8_REG_CLR.sd_dat_2_0
 * - clears function_select bits of SD_DAT_2_0 pad
 * clears function_select bits of SD_DAT_2_0 pad
 * 001 : sd.sd_dat_2_0 */
#define REG_3_SD_DAT_2_0_SHIFT			8
#define REG_3_SD_DAT_2_0_WIDTH			3
#define REG_3_SD_DAT_2_0_MASK			0x00000700
#define REG_3_SD_DAT_2_0_INV_MASK		0xFFFFF8FF
#define REG_3_SD_DAT_2_0_HW_DEFAULT		0x0

/* SW_TOP_FUNC_SEL_8_REG_CLR.sd_dat_2_1
 * - clears function_select bits of SD_DAT_2_1 pad
 * clears function_select bits of SD_DAT_2_1 pad
 * 001 : sd.sd_dat_2_1 */
#define REG_3_SD_DAT_2_1_SHIFT			12
#define REG_3_SD_DAT_2_1_WIDTH			3
#define REG_3_SD_DAT_2_1_MASK			0x00007000
#define REG_3_SD_DAT_2_1_INV_MASK		0xFFFF8FFF
#define REG_3_SD_DAT_2_1_HW_DEFAULT		0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.sd_dat_2_2
 * - sets function_select bits of SD_DAT_2_2 pad
 * sets function_select bits of SD_DAT_2_2 pad
 * 001 : sd.sd_dat_2_2 */
#define REG_3_SD_DAT_2_2_SHIFT			16
#define REG_3_SD_DAT_2_2_WIDTH			3
#define REG_3_SD_DAT_2_2_MASK			0x00070000
#define REG_3_SD_DAT_2_2_INV_MASK		0xFFF8FFFF
#define REG_3_SD_DAT_2_2_HW_DEFAULT		0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.sd_dat_2_3
 * - sets function_select bits of SD_DAT_2_3 pad
 * sets function_select bits of SD_DAT_2_3 pad
 * 001 : sd.sd_dat_2_3 */
#define REG_3_SD_DAT_2_3_SHIFT			20
#define REG_3_SD_DAT_2_3_WIDTH			3
#define REG_3_SD_DAT_2_3_MASK			0x00700000
#define REG_3_SD_DAT_2_3_INV_MASK		0xFF8FFFFF
#define REG_3_SD_DAT_2_3_HW_DEFAULT		0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.sd_clk_2
 * - sets function_select bits of SD_CLK_2 pad
 * sets function_select bits of SD_CLK_2 pad
 * 001 : sd.sd_clk_2 */
#define REG_3_SD_CLK_2_SHIFT			0
#define REG_3_SD_CLK_2_WIDTH			3
#define REG_3_SD_CLK_2_MASK			0x00000007
#define REG_3_SD_CLK_2_INV_MASK			0xFFFFFFF8
#define REG_3_SD_CLK_2_HW_DEFAULT		0x0

#define SD_CLK_2_DRIVESTRENGTH_MASK		0xf
#define	SD_2_PULL_MASK				0xFFF
enum FUNC_CEL_OPTION {
	REG_3_SD_CMD_2 = 0,
	REG_3_SD_DATA_2_0 = 1,
	REG_3_SD_DATA_2_1 = 2,
	REG_3_SD_DATA_2_2 = 3,
	REG_3_SD_DATA_2_3 = 4,
	REG_3_SD_CLK_2 = 5,
	REG_16_SD_CD_B_2 = 6,
	REG_16_SD_WP_B_2 = 7
};

#define IOC_RTC_BASE				    0x18880000
#define IOC_RTC_REG(offset)		        (IOC_RTC_BASE + (offset))
#define SW_RTC_IN_DISABLE_1_REG_CLR     IOC_RTC_REG(0x0A0C)
#define SW_RTC_IN_DISABLE_1_REG_CLR_SD2__SD_CD_B_2 (1 << 7)

#define IOC_TOP_FUNC_I2C0			0x110000
#define IOC_TOP_FUNC_I2C1			0x44000000
#endif /* CONFIG_ARCH_ALTAS7 */

#endif
