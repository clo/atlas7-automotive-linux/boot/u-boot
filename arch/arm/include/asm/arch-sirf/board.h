/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_BOARD_H
#define __SIRF_BOARD_H

void sirfsoc_rtc_iobrg_writel(u32 val, u32 addr);
u32 sirfsoc_rtc_iobrg_readl(u32 addr);
bool is_resume_required(void);
void printch(char);

bool i2c_dev_reg_bit_clear(int port_index, u32 dev_addr, u32 reg_addr,
			   u8 bit_map);
void sirfsoc_set_watchdog(u32 timeout, int enable);
void spi_init(void);
void sirf_dm9000_hw_init(void);
u32 sirfsoc_rtc_iobrg_unsafe_readl(u32 addr);
void sirfsoc_rtc_iobrg_unsafe_writel(u32 val, u32 addr);

#endif
