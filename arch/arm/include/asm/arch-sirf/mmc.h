/*
 * Copyright (c) 2012, 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_MMC_H
#define __SIRF_MMC_H

#ifdef CONFIG_ARCH_MARCO
#define SD_MODULE_BASE			0xCD000000
#else
#define SD_MODULE_BASE			0x56000000
#endif

#ifdef CONFIG_ARCH_ATLAS7
#define SD_REG_BASE(index) ((index < 2) ? \
		(0x16000000 + (index) * 0x00100000) : \
		(0x14000000 + (index) * 0x00100000))
#else
#define SD_REG_BASE(index)		(SD_MODULE_BASE + (index) * 0x00100000)
#endif

int sirf_sdhci_init(u32 regbase, u32 max_clk, u32 min_clk, u32 quirks);

#endif /* __SIRF_MMC_H */
