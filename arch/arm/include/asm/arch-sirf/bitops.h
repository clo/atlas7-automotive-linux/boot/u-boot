/*
 * Copyright (c) 2012, 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_BITOPS_H
#define __SIRF_BITOPS_H

#include <common.h>
#include <errno.h>
#include <asm/io.h>

inline unsigned int get_bit(void *addr, unsigned int mask)
{
	return readl(addr) & mask;
}

/* ustt: us times 10 */
int
on_bit(void *addr, unsigned int mask, int flag, int ustt)
{
	int time = ustt;

	for (;;) {
		if (flag) {
			if (readl(addr) & mask)
				return 0;
		} else {
			if (!(readl(addr) & mask))
				return 0;
		}
		if (time-- > 0)
			udelay(10);
		else
			return -ETIME;
	}
}

inline int on_bit_clear(void *addr, unsigned int mask, int ustt)
{
	return on_bit(addr, mask, 0, ustt);
}

inline int on_bit_set(void *addr, unsigned int mask, int ustt)
{
	return on_bit(addr, mask, 1, ustt);
}

/* ustt: us times 10 */
#endif
