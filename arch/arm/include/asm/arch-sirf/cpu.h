/*
 * Copyright (c) 2012-2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_CPU_H
#define __SIRF_CPU_H

#define MEMC_PHY_00_OFFSET              0x400
#define MEMC_PHY_00                     0x90000400
#define MEMC_PHY_DX0DQSTR_OFFSET        0x11d4
#define MEMC_PHY_DX0DQSTR               0x900011d4
#define MEMC_PHY_DX1DQSTR_OFFSET        0x1214
#define MEMC_PHY_DX1DQSTR               0x90001214
#define MEMC_PHY_DX2DQSTR_OFFSET        0x1254
#define MEMC_PHY_DX2DQSTR               0x90001254
#define MEMC_PHY_DX3DQSTR_OFFSET        0x1294
#define MEMC_PHY_DX3DQSTR               0x90001294
#define MEMC_PHY_PGCR_OFFSET            0x1008
#define MEMC_PHY_PGCR                   0x90001008
#define DENALI_CTL_00_OFFSET            0x0000
#define DENALI_CTL_00                   0x90000000

#ifdef CONFIG_ARCH_MARCO
#define CPU_IO_BRIDGE			0xC1000000
#elif defined(CONFIG_ARCH_ATLAS7)
#define CPU_IO_BRIDGE			0x18840000
#else
#define CPU_IO_BRIDGE			0x80030000
#endif
#define CPUIOBRG_CTRL			0x00
#define CPUIOBRG_WRBE			0x04
#define CPUIOBRG_ADDR			0x08
#define CPUIOBRG_DATA			0x0c

#define PWRC_REG(x)             (x + SYS_PWRC_BASE)
#define SYS_PWRC_BASE			0x3000
#define PWRC_BASE_PHYS			(CPU_IO_BRIDGE + SYS_PWRC_BASE)
#define PWRC_PDN_CTRL			0x0
#ifdef CONFIG_ARCH_ATLAS7
#define PWRC_PON_STATUS			0x8
#define PWRC_PDN_CTRL_SET       0x0
#define PWRC_PDN_CTRL_CLR		0x4
#define PWRC_PDN_FORCE_SHUTDOWN_TIME                     7
#define PWRC_PDN_WATCHDOG_STOP                           (1 << 9)
#define PWRC_PDN_ONKEY_WATCHDOG_EN                       (1 << 10)
#define PWRC_PDN_EXTON_WATCHDOG_EN                       (0 << 11)
#define PWRC_RTC_DCOG           0x7C
#define PWRC_RTC_SHUTDOWN_EN                             (1 << 10)

#else
#define PWRC_PON_STATUS			0x4
#endif
#define PWRC_SCRATCH_PAD1		0x18
#define PWRC_SCRATCH_PAD3		0x20
#define PWRC_WARM_BOOT_BIT		0x1
#define PWRC_DRAM_HOLD_BIT		0x8
#define PWRC_PDN_START_BIT		0x1
#define PWRC_PDN_MODE_BIT		0x6

#define KERNEL_BOOTING			(1 << 0)
#define KERNEL_BOOTING_FAIL		(1 << 1)
#define RECOVERY_BOOTING		(1 << 2)
#define RECOVERY_BOOTING_FAIL		(1 << 3)
#define RECOVERY_MODE			(1 << 4)
#define RECOVERY_SECURE			(1 << 5)
#define MP_MODE_BOOT			(1 << 6)
#define RECOVERY_SECURE_FAIL		0x3c

#ifdef CONFIG_ARCH_ATLAS7

#define CPU_REG_BASE            0x10230000
#define CPU_REG(offset)			(CPU_REG_BASE + (offset))

#define CPU_BOOT_CFG            CPU_REG(0x001C)
#define M3_BOOT_TYPE_INDEP		0x10
#define M3_SRAM_BASE			0x17E00000
#define M3_ROM_BASE			0x17E20000

#define PWRC_FAST_DOMAIN_BASE           0x188D0000

#define PWRC_REG_F(offset)              (PWRC_FAST_DOMAIN_BASE + (offset))
#define PWRC_SCRATCH_PAD1_F             PWRC_REG_F(0x0000)
#define PWRC_SCRATCH_PAD2_F				PWRC_REG_F(0x0004)
#define PWRC_SCRATCH_PAD3_F				PWRC_REG_F(0x0008)
#define PWRC_SCRATCH_PAD4_F				PWRC_REG_F(0x000C)
#define PWRC_SCRATCH_PAD5_F				PWRC_REG_F(0x0010)
#define PWRC_SCRATCH_PAD6_F				PWRC_REG_F(0x0014)
#define PWRC_SCRATCH_PAD7_F				PWRC_REG_F(0x0018)
#define PWRC_SCRATCH_PAD8_F				PWRC_REG_F(0x001C)
#define PWRC_SCRATCH_PAD9_F				PWRC_REG_F(0x0020)
#define PWRC_SCRATCH_PAD10_F			PWRC_REG_F(0x0024)
#define PWRC_SCRATCH_PAD11_F			PWRC_REG_F(0x0028)
#define PWRC_SCRATCH_PAD12_F			PWRC_REG_F(0x002C)
#define PWRC_RTC_BOOTSTRAP0_F			PWRC_REG_F(0x0030)
#define PWRC_RTC_BOOTSTRAP1_F			PWRC_REG_F(0x0034)
#define PWRC_M3_HASH_0_F				PWRC_REG_F(0x0038)
#define PWRC_M3_HASH_1_F				PWRC_REG_F(0x003C)
#define PWRC_M3_HASH_2_F				PWRC_REG_F(0x0040)
#define PWRC_M3_HASH_3_F				PWRC_REG_F(0x0044)
#define PWRC_M3_HASH_4_F				PWRC_REG_F(0x0048)
#define PWRC_M3_HASH_5_F				PWRC_REG_F(0x004C)
#define PWRC_M3_HASH_6_F				PWRC_REG_F(0x0050)
#define PWRC_M3_HASH_7_F				PWRC_REG_F(0x0054)
#define PWRC_QSPI_PARAMS_F				PWRC_REG_F(0x0058)
#define PWRC_CSSI_LPI_CTRL_F			PWRC_REG_F(0x005C)
#define PWRC_BCRL_VAL_F					PWRC_REG_F(0x0060)
#define PWRC_RTCM_AMP_ADJUST_F			PWRC_REG_F(0x0064)
#define PWRC_KAS_SW_CONFIG_F			PWRC_REG_F(0x0068)
#define PWRC_RTC_GNSS_SD_F				PWRC_REG_F(0x006C)
#define PWRC_RTC_W1_DEBUG_F				PWRC_REG_F(0x0070)
#define PWRC_RTC_IOB_CTRL_F				PWRC_REG_F(0x0074)
#define PWRC_SPINLOCK_RD_DEBUG			PWRC_REG_F(0x0078)
#define PWRC_TESTANDSET_0_F				PWRC_REG_F(0x007C)
#define PWRC_WATCHDOG_CONFIG_F			PWRC_REG_F(0x0080)


#endif
#ifdef CONFIG_ARCH_ATLAS7
#define SIRFSOC_BOOT_STATUS		PWRC_SCRATCH_PAD11_F
#else
#define SIRFSOC_BOOT_STATUS		(SYS_PWRC_BASE + PWRC_SCRATCH_PAD3)
#endif
#ifdef CONFIG_ARCH_MARCO
#define BOOTLOADER_ENTRY			0x40400000
#else
#define BOOTLOADER_ENTRY			0x00400000
#endif
#define BOOTLOADER_LOAD_ADRS		(BOOTLOADER_ENTRY - 0x800)
#define BOOTLOADER_STACK_TOP		(BOOTLOADER_ENTRY - 0x5000)

#ifdef CONFIG_ARCH_ATLAS7

#define SRAM_BASE					0x04000000
#define ROM_RETURN_ADRS				(SRAM_BASE + 0x18800)
#define ROM_LOAD_INDEX				(ROM_RETURN_ADRS + 0x4)
#define P_BOOTLOADER_LAUNCH_ADRS	(ROM_RETURN_ADRS + 0x8)
#define P_BOOTLOADER_LOAD_ADRS		0

/* M3 launch address */
#define M3_WAKEUP_FLAG_ADDR			0x1862046C
#define M3_WAKEUP_JUMP_ADDR			0x18620470
#define M3_WAKEUP_FLAG_VALID		0x5F8C6D3E


#elif CONFIG_ARCH_MARCO

#define SRAM_BASE					0xCA008000
#define ROM_RETURN_ADRS				(SRAM_BASE  - 0x400)
#define P_BOOTLOADER_LOAD_ADRS		(ROM_RETURN_ADRS + 0x4)
#define P_BOOTLOADER_LAUNCH_ADRS	(ROM_RETURN_ADRS + 0x8)

#else

#define SRAM_BASE					0xA8008000
#define ROM_RETURN_ADRS				(SRAM_BASE + 0x20)
#define P_BOOTLOADER_LOAD_ADRS		(SRAM_BASE + 0x24)
#define P_BOOTLOADER_LAUNCH_ADRS	(SRAM_BASE + 0x28)

#endif
#endif
