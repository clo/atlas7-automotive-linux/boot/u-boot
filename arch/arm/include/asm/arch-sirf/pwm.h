/*
 * Drivers for SiRF On-Chip pwm devices
 *
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _pwm_h_
#define _pwm_h_
struct pwm_device {
	unsigned char		pwm;
	unsigned char		bypass_mode;
	unsigned int		sigsrc_clk_idx;
	unsigned int		period_ns;	/* in nanoseconds */
	unsigned int		duty_ns;	/* in nanoseconds */
};

void sirf_pwm_config(struct pwm_device *pwm, int duty_ns, int period_ns);
void sirf_pwm_enable(struct pwm_device *pwm);
void sirf_pwm_disable(struct pwm_device *pwm);

void sirf_pwm_io_clock_enable(void);
void sirf_gpio_mux_to_pwm(void);
#endif /* _pwm_h_ */
