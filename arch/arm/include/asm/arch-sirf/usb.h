/*
 * Copyright (c) 2012, 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_USB_H
#define __SIRF_USB_H

#ifdef CONFIG_ARCH_MARCO
#define USB_REG_BASE(idx)		(0xCE000000 + (idx) * 0x10000)
#else
#ifdef CONFIG_ARCH_ATLAS7
#define USB_REG_BASE(idx)		(0x17060000 + (idx) * 0x10000)
#else
#define USB_REG_BASE(idx)		(0xb8000000 + (idx) * 0x10000)
#endif
#endif

#define HCCR_OFFSET			0x0100
#define HCOR_OFFSET			0x0140

#define USB_PHY_UTMI			1
#define USB_PHY_ULPI			0

/* Register Definitions */
#define USBOTG_USBCMD			0x0140
#define RS				(1 << 0)
#define RST				(1 << 1)

#define USBOTG_USBINTR			0x0148
#define PCE				(1 << 2)

#define	USBOTG_BURSTSIZE		0x160

#define USBOTG_PORTSC1			0x0184
#define PHCD				(1 << 23)

#define USBOTG_OTGSC			0x01A4
#define ID				(1 << 8)
#define IDIE				(1 << 24)
#define AVVIE				(1 << 25)
#define ASVIE				(1 << 26)
#define BSVIE				(1 << 27)
#define BSEIE				(1 << 28)

#define USBOTG_USBMODE			0x01A8
#define USBOTG_MODE_DEVICE		(0x2 << 0)
#define USBOTG_MODE_HOST		(0x3 << 0)
#define USBOTG_MODE_SDIS		(1 << 4)

#define USBPHY_PROGRAM			0x200
#define USBPHY_DRVVBUS			(1 << 31)
#define USBPHY_AXI_BURST_LENGTH		(1 << 0)
#define USBPHY_POR			(1 << 27)

struct usbcd_db {
	void	*regbase;
	u8	phy;
	u32	hccr;
	u32	hcor;
};

#endif
