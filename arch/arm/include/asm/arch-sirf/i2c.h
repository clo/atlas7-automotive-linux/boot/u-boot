/*
 * Copyright (c) 2012, 2014-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_I2C_H
#define __SIRF_I2C_H

#ifdef CONFIG_ARCH_ATLAS7
#define I2C0_MODULE_BASE			0X17020000
#define I2C1_MODULE_BASE			0x17030000
#else
#define I2C0_MODULE_BASE			0xB00E0000
#define I2C1_MODULE_BASE			0xB00F0000
#endif

/*
 * Mask definitions for I2C status register
 */
#define I2C_STATUS_BUSY				(0x1 << 0)
#define I2C_STATUS_TRANSFERING			(0x1 << 1)
#define I2C_STATUS_NACK				(0x1 << 2)
#define	I2C_STATUS_CMD_FINISH_INT		(0x1 << 8)
#define	I2C_STATUS_ERROR_INT			(0x1 << 9)
#define	I2C_STATUS_CMD_INDEX_MASK		(0xf << 16)

/*
 * Mask definitions for I2C control register
 */
#define I2C_CTRL_RESET				(0x1 << 0)
#define I2C_CTRL_CORE_EN			(0x1 << 1)
#define I2C_CTRL_MODE_M				(0x1 << 2)
#define I2C_CTRL_CMD_FINISH_INT_EN		(0x1 << 11)
#define	I2C_CTRL_ERROR_INT_EN			(0x1 << 12)

/*
 * Mask definitions for I2C CMD register
 */
#define	I2C_CMD_RP_NUM_MASK			(0x7 << 0)
#define	I2C_CMD_NACK				(0x1 << 3)
#define	I2C_CMD_WRITE				(0x1 << 4)
#define	I2C_CMD_READ				(0x1 << 5)
#define	I2C_CMD_SEND_STOP			(0x1 << 6)
#define	I2C_CMD_SEND_START			(0x1 << 7)

/*
 * Mask definitions for I2C line input filter of SDA delay
 */
#define	I2C_NO_FILTER				(0x0 << 8)
#define	I2C_3_IOCLK_FILTER			(0x1 << 8)
#define	I2C_6_IOCLK_FILTER			(0x2 << 8)
#define	I2C_10_IOCLK_FILTER			(0x3 << 8)

bool i2c_init(int, u32);
bool i2c_read(int port_index, u32 dev_addr,
	u32 reg_addr, u8 *bit_map);
bool i2c_write(int port_index, u32 dev_addr,
	u32 reg_addr, u8 bit_map);
bool i2c_dev_reg_read(int port_index, u32 dev_addr,
	u32 reg_addr, u8 *bit_map);


#endif
