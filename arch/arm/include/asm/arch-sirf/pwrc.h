/*
 * Copyright (c) 2014-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PWRC_H_
#define __PWRC_H_

#ifdef CONFIG_ARCH_ATLAS7

#define _RETAIN_BASE	0x188D0000

#define RETAIN_REG(offset)		(_RETAIN_BASE + (offset))

#define A7DA_PWRC_SCRATCH_PAD11		RETAIN_REG(0x0028)
#define A7DA_PWRC_TESTANDSET_0		RETAIN_REG(0x0078)

#define request_hwlock()        while (!readl(A7DA_PWRC_TESTANDSET_0))
#define release_hwlock()         writel(1, A7DA_PWRC_TESTANDSET_0)

#define PWRC_IS_CLOCK_LATE_INITIALIZED()	\
	(0 != (readl(A7DA_PWRC_SCRATCH_PAD11) & BIT(29)))
#define PWRC_SET_CLOCK_LATE_INITIALIZED()             \
	writel(readl(A7DA_PWRC_SCRATCH_PAD11) | BIT(29), \
			A7DA_PWRC_SCRATCH_PAD11)

#define PWRC_IS_CLOCK_INITIALIZED()	\
	(0 != (readl(A7DA_PWRC_SCRATCH_PAD11) & BIT(30)))
#define PWRC_SET_CLOCK_INITIALIZED()	\
	writel(readl(A7DA_PWRC_SCRATCH_PAD11) | BIT(30), \
			A7DA_PWRC_SCRATCH_PAD11)

#define PWRC_IS_DDR_INITIALIZED()	\
	(0 != (readl(A7DA_PWRC_SCRATCH_PAD11) & BIT(31)))
#define PWRC_SET_DDR_INITIALIZED()	\
	writel(readl(A7DA_PWRC_SCRATCH_PAD11) | BIT(31), \
			A7DA_PWRC_SCRATCH_PAD11)
#endif

#endif
